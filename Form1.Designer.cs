﻿namespace thunderbolt_tx_automation
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.TEST_FLOW = new System.Windows.Forms.ListBox();
            this.custom = new System.Windows.Forms.RadioButton();
            this.Default = new System.Windows.Forms.RadioButton();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.test1 = new System.Windows.Forms.CheckBox();
            this.test11 = new System.Windows.Forms.CheckBox();
            this.test9 = new System.Windows.Forms.CheckBox();
            this.test8 = new System.Windows.Forms.CheckBox();
            this.test6 = new System.Windows.Forms.CheckBox();
            this.test5 = new System.Windows.Forms.CheckBox();
            this.test4 = new System.Windows.Forms.CheckBox();
            this.test3 = new System.Windows.Forms.CheckBox();
            this.test2 = new System.Windows.Forms.CheckBox();
            this.run = new System.Windows.Forms.Button();
            this.tgl_mode = new System.Windows.Forms.RadioButton();
            this.none_tgl_mode = new System.Windows.Forms.RadioButton();
            this.Log = new System.Windows.Forms.ListBox();
            this.test10 = new System.Windows.Forms.CheckBox();
            this.visa_name = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.port_a = new System.Windows.Forms.RadioButton();
            this.port_b = new System.Windows.Forms.RadioButton();
            this.port_c = new System.Windows.Forms.RadioButton();
            this.port_d = new System.Windows.Forms.RadioButton();
            this.group_port = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.group_lan = new System.Windows.Forms.GroupBox();
            this.lan0 = new System.Windows.Forms.CheckBox();
            this.lan1 = new System.Windows.Forms.CheckBox();
            this.button3 = new System.Windows.Forms.Button();
            this.group_freq = new System.Windows.Forms.GroupBox();
            this.Freq10G = new System.Windows.Forms.CheckBox();
            this.Freq10p3G = new System.Windows.Forms.CheckBox();
            this.Freq20G = new System.Windows.Forms.CheckBox();
            this.Freq20p6G = new System.Windows.Forms.CheckBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.test7 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.group_port.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.group_lan.SuspendLayout();
            this.group_freq.SuspendLayout();
            this.SuspendLayout();
            // 
            // TEST_FLOW
            // 
            this.TEST_FLOW.FormattingEnabled = true;
            this.TEST_FLOW.ItemHeight = 12;
            this.TEST_FLOW.Location = new System.Drawing.Point(138, 121);
            this.TEST_FLOW.Name = "TEST_FLOW";
            this.TEST_FLOW.Size = new System.Drawing.Size(81, 148);
            this.TEST_FLOW.TabIndex = 3;
            this.TEST_FLOW.SelectedIndexChanged += new System.EventHandler(this.TEST_FLOW_SelectedIndexChanged);
            // 
            // custom
            // 
            this.custom.AutoSize = true;
            this.custom.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.custom.Location = new System.Drawing.Point(114, 17);
            this.custom.Name = "custom";
            this.custom.Size = new System.Drawing.Size(74, 20);
            this.custom.TabIndex = 2;
            this.custom.Text = "Custom";
            this.custom.UseVisualStyleBackColor = true;
            this.custom.CheckedChanged += new System.EventHandler(this.custom_CheckedChanged);
            // 
            // Default
            // 
            this.Default.AutoSize = true;
            this.Default.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Default.Location = new System.Drawing.Point(5, 17);
            this.Default.Name = "Default";
            this.Default.Size = new System.Drawing.Size(72, 20);
            this.Default.TabIndex = 1;
            this.Default.Text = "Default";
            this.Default.UseVisualStyleBackColor = true;
            this.Default.CheckedChanged += new System.EventHandler(this.Default_CheckedChanged);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // test1
            // 
            this.test1.AutoSize = true;
            this.test1.Location = new System.Drawing.Point(29, 118);
            this.test1.Name = "test1";
            this.test1.Size = new System.Drawing.Size(51, 16);
            this.test1.TabIndex = 4;
            this.test1.Text = "Preset";
            this.test1.UseVisualStyleBackColor = true;
            this.test1.CheckedChanged += new System.EventHandler(this.test1_CheckedChanged);
            // 
            // test11
            // 
            this.test11.AutoSize = true;
            this.test11.Location = new System.Drawing.Point(29, 306);
            this.test11.Name = "test11";
            this.test11.Size = new System.Drawing.Size(85, 16);
            this.test11.TabIndex = 13;
            this.test11.Text = "Jitter_Passive";
            this.test11.UseVisualStyleBackColor = true;
            this.test11.CheckedChanged += new System.EventHandler(this.test11_CheckedChanged);
            // 
            // test9
            // 
            this.test9.AutoSize = true;
            this.test9.Location = new System.Drawing.Point(29, 262);
            this.test9.Name = "test9";
            this.test9.Size = new System.Drawing.Size(70, 16);
            this.test9.TabIndex = 12;
            this.test9.Text = "CLK_SW";
            this.test9.UseVisualStyleBackColor = true;
            // 
            // test8
            // 
            this.test8.AutoSize = true;
            this.test8.Location = new System.Drawing.Point(29, 242);
            this.test8.Name = "test8";
            this.test8.Size = new System.Drawing.Size(57, 16);
            this.test8.TabIndex = 11;
            this.test8.Text = "EIDLE";
            this.test8.UseVisualStyleBackColor = true;
            this.test8.CheckedChanged += new System.EventHandler(this.test8_CheckedChanged);
            // 
            // test6
            // 
            this.test6.AutoSize = true;
            this.test6.Location = new System.Drawing.Point(29, 220);
            this.test6.Name = "test6";
            this.test6.Size = new System.Drawing.Size(46, 16);
            this.test6.TabIndex = 9;
            this.test6.Text = "Jitter";
            this.test6.UseVisualStyleBackColor = true;
            // 
            // test5
            // 
            this.test5.AutoSize = true;
            this.test5.Location = new System.Drawing.Point(29, 200);
            this.test5.Name = "test5";
            this.test5.Size = new System.Drawing.Size(46, 16);
            this.test5.TabIndex = 8;
            this.test5.Text = "EYE";
            this.test5.UseVisualStyleBackColor = true;
            // 
            // test4
            // 
            this.test4.AutoSize = true;
            this.test4.Location = new System.Drawing.Point(29, 180);
            this.test4.Name = "test4";
            this.test4.Size = new System.Drawing.Size(44, 16);
            this.test4.TabIndex = 7;
            this.test4.Text = "SSC";
            this.test4.UseVisualStyleBackColor = true;
            // 
            // test3
            // 
            this.test3.AutoSize = true;
            this.test3.Location = new System.Drawing.Point(29, 160);
            this.test3.Name = "test3";
            this.test3.Size = new System.Drawing.Size(58, 16);
            this.test3.TabIndex = 6;
            this.test3.Text = "ACCM";
            this.test3.UseVisualStyleBackColor = true;
            // 
            // test2
            // 
            this.test2.AutoSize = true;
            this.test2.Location = new System.Drawing.Point(29, 140);
            this.test2.Name = "test2";
            this.test2.Size = new System.Drawing.Size(52, 16);
            this.test2.TabIndex = 5;
            this.test2.Text = "TRTF";
            this.test2.UseVisualStyleBackColor = true;
            this.test2.CheckedChanged += new System.EventHandler(this.test2_CheckedChanged);
            // 
            // run
            // 
            this.run.BackColor = System.Drawing.Color.LightCyan;
            this.run.Location = new System.Drawing.Point(13, 420);
            this.run.Name = "run";
            this.run.Size = new System.Drawing.Size(102, 42);
            this.run.TabIndex = 67;
            this.run.Text = "Run";
            this.run.UseVisualStyleBackColor = false;
            this.run.Click += new System.EventHandler(this.run_Click);
            // 
            // tgl_mode
            // 
            this.tgl_mode.AutoSize = true;
            this.tgl_mode.Location = new System.Drawing.Point(5, 15);
            this.tgl_mode.Name = "tgl_mode";
            this.tgl_mode.Size = new System.Drawing.Size(98, 16);
            this.tgl_mode.TabIndex = 68;
            this.tgl_mode.TabStop = true;
            this.tgl_mode.Text = "TigerLakeMode";
            this.tgl_mode.UseVisualStyleBackColor = true;
            // 
            // none_tgl_mode
            // 
            this.none_tgl_mode.AutoSize = true;
            this.none_tgl_mode.Location = new System.Drawing.Point(105, 15);
            this.none_tgl_mode.Name = "none_tgl_mode";
            this.none_tgl_mode.Size = new System.Drawing.Size(127, 16);
            this.none_tgl_mode.TabIndex = 69;
            this.none_tgl_mode.TabStop = true;
            this.none_tgl_mode.Text = "None-TigerLakeMode";
            this.none_tgl_mode.UseVisualStyleBackColor = true;
            // 
            // Log
            // 
            this.Log.FormattingEnabled = true;
            this.Log.ItemHeight = 12;
            this.Log.Location = new System.Drawing.Point(306, 115);
            this.Log.Name = "Log";
            this.Log.Size = new System.Drawing.Size(303, 400);
            this.Log.TabIndex = 70;
            this.Log.SelectedIndexChanged += new System.EventHandler(this.Log_SelectedIndexChanged);
            // 
            // test10
            // 
            this.test10.AutoSize = true;
            this.test10.Location = new System.Drawing.Point(29, 284);
            this.test10.Name = "test10";
            this.test10.Size = new System.Drawing.Size(85, 16);
            this.test10.TabIndex = 71;
            this.test10.Text = "EYE_Passive";
            this.test10.UseVisualStyleBackColor = true;
            this.test10.CheckedChanged += new System.EventHandler(this.test10_CheckedChanged);
            // 
            // visa_name
            // 
            this.visa_name.Location = new System.Drawing.Point(12, 529);
            this.visa_name.Name = "visa_name";
            this.visa_name.Size = new System.Drawing.Size(235, 22);
            this.visa_name.TabIndex = 72;
            this.visa_name.TextChanged += new System.EventHandler(this.visa_name_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 511);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(130, 12);
            this.label1.TabIndex = 73;
            this.label1.Text = "USB VISA Resource name";
            // 
            // port_a
            // 
            this.port_a.AutoSize = true;
            this.port_a.Location = new System.Drawing.Point(23, 10);
            this.port_a.Name = "port_a";
            this.port_a.Size = new System.Drawing.Size(53, 16);
            this.port_a.TabIndex = 74;
            this.port_a.TabStop = true;
            this.port_a.Text = "Port A";
            this.port_a.UseVisualStyleBackColor = true;
            this.port_a.CheckedChanged += new System.EventHandler(this.port_a_CheckedChanged);
            // 
            // port_b
            // 
            this.port_b.AutoSize = true;
            this.port_b.Location = new System.Drawing.Point(82, 10);
            this.port_b.Name = "port_b";
            this.port_b.Size = new System.Drawing.Size(53, 16);
            this.port_b.TabIndex = 75;
            this.port_b.TabStop = true;
            this.port_b.Text = "Port B";
            this.port_b.UseVisualStyleBackColor = true;
            this.port_b.CheckedChanged += new System.EventHandler(this.port_b_CheckedChanged);
            // 
            // port_c
            // 
            this.port_c.AutoSize = true;
            this.port_c.Location = new System.Drawing.Point(141, 10);
            this.port_c.Name = "port_c";
            this.port_c.Size = new System.Drawing.Size(53, 16);
            this.port_c.TabIndex = 76;
            this.port_c.TabStop = true;
            this.port_c.Text = "Port C";
            this.port_c.UseVisualStyleBackColor = true;
            // 
            // port_d
            // 
            this.port_d.AutoSize = true;
            this.port_d.Location = new System.Drawing.Point(200, 10);
            this.port_d.Name = "port_d";
            this.port_d.Size = new System.Drawing.Size(53, 16);
            this.port_d.TabIndex = 77;
            this.port_d.TabStop = true;
            this.port_d.Text = "Port D";
            this.port_d.UseVisualStyleBackColor = true;
            this.port_d.CheckedChanged += new System.EventHandler(this.port_d_CheckedChanged);
            // 
            // group_port
            // 
            this.group_port.Controls.Add(this.port_a);
            this.group_port.Controls.Add(this.port_b);
            this.group_port.Controls.Add(this.port_c);
            this.group_port.Controls.Add(this.port_d);
            this.group_port.Location = new System.Drawing.Point(306, 77);
            this.group_port.Name = "group_port";
            this.group_port.Size = new System.Drawing.Size(303, 32);
            this.group_port.TabIndex = 80;
            this.group_port.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.Default);
            this.groupBox3.Controls.Add(this.custom);
            this.groupBox3.Location = new System.Drawing.Point(24, 8);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(236, 46);
            this.groupBox3.TabIndex = 82;
            this.groupBox3.TabStop = false;
            this.groupBox3.Enter += new System.EventHandler(this.groupBox3_Enter);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.tgl_mode);
            this.groupBox4.Controls.Add(this.none_tgl_mode);
            this.groupBox4.Location = new System.Drawing.Point(24, 59);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(236, 37);
            this.groupBox4.TabIndex = 83;
            this.groupBox4.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Vladimir Script", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(563, 532);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 84;
            this.label2.Text = "Shawn Li";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.LightCyan;
            this.button1.Location = new System.Drawing.Point(306, 527);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 85;
            this.button1.Text = "Clear Log";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.MediumAquamarine;
            this.button2.Location = new System.Drawing.Point(173, 499);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 86;
            this.button2.Text = "Test";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // group_lan
            // 
            this.group_lan.Controls.Add(this.lan0);
            this.group_lan.Controls.Add(this.lan1);
            this.group_lan.Location = new System.Drawing.Point(306, 11);
            this.group_lan.Name = "group_lan";
            this.group_lan.Size = new System.Drawing.Size(303, 32);
            this.group_lan.TabIndex = 81;
            this.group_lan.TabStop = false;
            // 
            // lan0
            // 
            this.lan0.AutoSize = true;
            this.lan0.Location = new System.Drawing.Point(22, 10);
            this.lan0.Name = "lan0";
            this.lan0.Size = new System.Drawing.Size(53, 16);
            this.lan0.TabIndex = 81;
            this.lan0.Text = "LAN0";
            this.lan0.UseVisualStyleBackColor = true;
            // 
            // lan1
            // 
            this.lan1.AutoSize = true;
            this.lan1.Location = new System.Drawing.Point(82, 10);
            this.lan1.Name = "lan1";
            this.lan1.Size = new System.Drawing.Size(53, 16);
            this.lan1.TabIndex = 80;
            this.lan1.Text = "LAN1";
            this.lan1.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.LightCyan;
            this.button3.Location = new System.Drawing.Point(183, 420);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(102, 42);
            this.button3.TabIndex = 87;
            this.button3.Text = "Matlab analysis";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // group_freq
            // 
            this.group_freq.Controls.Add(this.Freq10G);
            this.group_freq.Controls.Add(this.Freq10p3G);
            this.group_freq.Controls.Add(this.Freq20G);
            this.group_freq.Controls.Add(this.Freq20p6G);
            this.group_freq.Location = new System.Drawing.Point(306, 43);
            this.group_freq.Name = "group_freq";
            this.group_freq.Size = new System.Drawing.Size(303, 32);
            this.group_freq.TabIndex = 88;
            this.group_freq.TabStop = false;
            // 
            // Freq10G
            // 
            this.Freq10G.AutoSize = true;
            this.Freq10G.Location = new System.Drawing.Point(22, 11);
            this.Freq10G.Name = "Freq10G";
            this.Freq10G.Size = new System.Drawing.Size(44, 16);
            this.Freq10G.TabIndex = 83;
            this.Freq10G.Text = "10G";
            this.Freq10G.UseVisualStyleBackColor = true;
            // 
            // Freq10p3G
            // 
            this.Freq10p3G.AutoSize = true;
            this.Freq10p3G.Location = new System.Drawing.Point(82, 11);
            this.Freq10p3G.Name = "Freq10p3G";
            this.Freq10p3G.Size = new System.Drawing.Size(53, 16);
            this.Freq10p3G.TabIndex = 82;
            this.Freq10p3G.Text = "10.3G";
            this.Freq10p3G.UseVisualStyleBackColor = true;
            // 
            // Freq20G
            // 
            this.Freq20G.AutoSize = true;
            this.Freq20G.Location = new System.Drawing.Point(150, 11);
            this.Freq20G.Name = "Freq20G";
            this.Freq20G.Size = new System.Drawing.Size(44, 16);
            this.Freq20G.TabIndex = 80;
            this.Freq20G.Text = "20G";
            this.Freq20G.UseVisualStyleBackColor = true;
            // 
            // Freq20p6G
            // 
            this.Freq20p6G.AutoSize = true;
            this.Freq20p6G.Location = new System.Drawing.Point(200, 11);
            this.Freq20p6G.Name = "Freq20p6G";
            this.Freq20p6G.Size = new System.Drawing.Size(53, 16);
            this.Freq20p6G.TabIndex = 81;
            this.Freq20p6G.Text = "20.6G";
            this.Freq20p6G.UseVisualStyleBackColor = true;
            this.Freq20p6G.CheckedChanged += new System.EventHandler(this.Freq20G_CheckedChanged);
            // 
            // test7
            // 
            this.test7.AutoSize = true;
            this.test7.Location = new System.Drawing.Point(29, 326);
            this.test7.Name = "test7";
            this.test7.Size = new System.Drawing.Size(39, 16);
            this.test7.TabIndex = 89;
            this.test7.Text = "EQ";
            this.test7.UseVisualStyleBackColor = true;
            this.test7.CheckedChanged += new System.EventHandler(this.test7_CheckedChanged_1);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(29, 346);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(55, 16);
            this.checkBox1.TabIndex = 90;
            this.checkBox1.Text = "EQ_Ps";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(29, 368);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(74, 16);
            this.checkBox2.TabIndex = 91;
            this.checkBox2.Text = "EQ_Ps_De";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(29, 390);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(58, 16);
            this.checkBox3.TabIndex = 92;
            this.checkBox3.Text = "EQ_De";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(228)))), ((int)(((byte)(163)))));
            this.ClientSize = new System.Drawing.Size(616, 558);
            this.Controls.Add(this.checkBox3);
            this.Controls.Add(this.checkBox2);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.test7);
            this.Controls.Add(this.group_freq);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.group_lan);
            this.Controls.Add(this.group_port);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.visa_name);
            this.Controls.Add(this.test10);
            this.Controls.Add(this.Log);
            this.Controls.Add(this.run);
            this.Controls.Add(this.test1);
            this.Controls.Add(this.test11);
            this.Controls.Add(this.test9);
            this.Controls.Add(this.test8);
            this.Controls.Add(this.test6);
            this.Controls.Add(this.test5);
            this.Controls.Add(this.test4);
            this.Controls.Add(this.test3);
            this.Controls.Add(this.test2);
            this.Controls.Add(this.TEST_FLOW);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Thunderbolt";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.group_port.ResumeLayout(false);
            this.group_port.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.group_lan.ResumeLayout(false);
            this.group_lan.PerformLayout();
            this.group_freq.ResumeLayout(false);
            this.group_freq.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox TEST_FLOW;
        private System.Windows.Forms.RadioButton custom;
        private System.Windows.Forms.RadioButton Default;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.CheckBox test1;
        private System.Windows.Forms.CheckBox test11;
        private System.Windows.Forms.CheckBox test9;
        private System.Windows.Forms.CheckBox test8;
        private System.Windows.Forms.CheckBox test6;
        private System.Windows.Forms.CheckBox test5;
        private System.Windows.Forms.CheckBox test4;
        private System.Windows.Forms.CheckBox test3;
        private System.Windows.Forms.CheckBox test2;
        private System.Windows.Forms.Button run;
        private System.Windows.Forms.RadioButton tgl_mode;
        private System.Windows.Forms.RadioButton none_tgl_mode;
        private System.Windows.Forms.ListBox Log;
        private System.Windows.Forms.CheckBox test10;
        private System.Windows.Forms.TextBox visa_name;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton port_a;
        private System.Windows.Forms.RadioButton port_b;
        private System.Windows.Forms.RadioButton port_c;
        private System.Windows.Forms.RadioButton port_d;
        private System.Windows.Forms.GroupBox group_port;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox group_lan;
        private System.Windows.Forms.CheckBox lan0;
        private System.Windows.Forms.CheckBox lan1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.GroupBox group_freq;
        private System.Windows.Forms.CheckBox Freq20p6G;
        private System.Windows.Forms.CheckBox Freq20G;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.CheckBox Freq10G;
        private System.Windows.Forms.CheckBox Freq10p3G;
        private System.Windows.Forms.CheckBox test7;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox3;
    }
}

