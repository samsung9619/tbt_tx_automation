﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using NationalInstruments.Visa;
using System.Diagnostics;
using System.Threading;
using System.Runtime.InteropServices;
using Excel = Microsoft.Office.Interop.Excel;
using System.IO;
namespace thunderbolt_tx_automation
{
    public partial class Form1 : Form
    {
        
        private MessageBasedSession mbSession_DSAZ334A;
        string file_path;
        string now_time;
        string PDER_value;
        string DSAZ334A_query_value;
        string DC_gain_20G;
        string DC_gain_10G;
        int checkbox_check_total_before = 0;
        int checkbox_check_total = 0;
        int DSAZ334A_isrun;
        Thread thread_for_testing;
        Thread analysising;

        //public string r6522_read_value;
        public Form1()
        {
            InitializeComponent();
        }
        private void Default_CheckedChanged(object sender, EventArgs e)
        {
            test1.Checked = true;
            test2.Checked = true;
            test3.Checked = true;
            test4.Checked = true;
            test5.Checked = true;
            test6.Checked = true;
            test7.Checked = true;
            test8.Checked = true;
            test9.Checked = true;
            test10.Checked = true;
            test11.Checked = true;

        }

        private void custom_CheckedChanged(object sender, EventArgs e)
        {
            TEST_FLOW.Items.Clear();
            foreach (Control c in Controls)
            {
                if (c is CheckBox)
                {
                    CheckBox cb = (CheckBox)c;
                    if (cb.Checked == true)
                    {
                        cb.Checked = false;
                    }
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            string[] getcheck_items = new string[12];
            int i;
            getcheck_items[0] = "0";
            checkbox_check_total = 0;
            foreach (Control c in Controls)
            {
                if (c is CheckBox)
                {
                    CheckBox cb = (CheckBox)c;
                    if (cb.Checked == true)
                    {
                        i = Convert.ToInt32(Regex.Replace(cb.Name, "[^0-9]", "")); 
                        getcheck_items[i] = cb.Text;
                        checkbox_check_total = checkbox_check_total + 1;
                    }
                    else
                    {
                        checkbox_check_total = checkbox_check_total - 1;
                    }
                }
            }
            
            if(checkbox_check_total_before != checkbox_check_total)
            {
                TEST_FLOW.Items.Clear();
                foreach (string item in getcheck_items)
                {
                    if (item != "0" && item != null)
                        TEST_FLOW.Items.Add(item);
                }                    
            }
            checkbox_check_total_before = checkbox_check_total;
            
            
            //if (test11.Checked == true)
            //{
            //    test10.Checked = true;
            //}
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            file_path = System.Windows.Forms.Application.StartupPath+"\\";
            tgl_mode.Checked = true;
            lan0.Checked = true;
            port_a.Checked = true;
            Default.Checked = true;
            tgl_mode.Checked = true;
            Freq10G.Checked = true;
            visa_name.Text = "USB0::0x2A8D::0x9065::MY55160109::INSTR";
        }

        void DSAZ334A(string gpib_address, string gpib_command)
        {
            var rmSession = new ResourceManager();
            try
            {
                mbSession_DSAZ334A = (MessageBasedSession)rmSession.Open(gpib_address);
                mbSession_DSAZ334A.RawIO.Write(gpib_command);
                //r6522_read_value = InsertCommonEscapeSequences(mbSession_DSAZ334A.RawIO.ReadString()).Substring(4, 6);
                mbSession_DSAZ334A.Dispose();
            }
            catch
            {
                MessageBox.Show("WARNING : Instrument address is wrong or Instrument is shut down");
                MessageBox.Show(gpib_command);
            }
        }

        void DSAZ334A_query(string gpib_address, string gpib_command)
        {
            var rmSession = new ResourceManager();
            try
            {
                mbSession_DSAZ334A = (MessageBasedSession)rmSession.Open(gpib_address);
                mbSession_DSAZ334A.RawIO.Write(gpib_command);
                //DSAZ334A_query_value = InsertCommonEscapeSequences(mbSession_DSAZ334A.RawIO.ReadString());
                DSAZ334A_query_value = (mbSession_DSAZ334A.RawIO.ReadString());
                mbSession_DSAZ334A.Dispose();
            }
            catch
            {
                MessageBox.Show("WARNING : Instrument address is wrong or Instrument is shut down");
            }
        }

        private void Check_DSAZ334AOLD_state(string gpib_address, string gpib_command)
        {
            var rmSession = new ResourceManager();
            try
            {
                mbSession_DSAZ334A = (MessageBasedSession)rmSession.Open(gpib_address);
                mbSession_DSAZ334A.RawIO.Write(gpib_command);
                Thread.Sleep(1000);
                PDER_value = InsertCommonEscapeSequences(mbSession_DSAZ334A.RawIO.ReadString());
                PDER_value = Regex.Replace(PDER_value, "[^0-9]", "");
            }
            catch
            {
                //MessageBox.Show("WARNING : Instrument address is wrong or Instrument is shut down");
            }
        }

        private void Check_DSAZ334A_state(string gpib_address, string gpib_command) //取代delay
        {
            var rmSession = new ResourceManager();
            try
            {
                mbSession_DSAZ334A = (MessageBasedSession)rmSession.Open(gpib_address);
                //mbSession_DSAZ334A.RawIO.Write(gpib_command);
                //PDER_value = mbSession_DSAZ334A.RawIO.ReadString();
                while (true)
                {
                    try
                    {
                        mbSession_DSAZ334A.RawIO.Write(gpib_command);
                        Thread.Sleep(1000);
                        PDER_value = mbSession_DSAZ334A.RawIO.ReadString();
                        PDER_value = Regex.Replace(PDER_value, "[^0-9]", "");
                        if (PDER_value == "1")
                        {
                            while (true)
                            {
                                mbSession_DSAZ334A.RawIO.Write(gpib_command);
                                Thread.Sleep(1000);
                                PDER_value = mbSession_DSAZ334A.RawIO.ReadString();
                                PDER_value = Regex.Replace(PDER_value, "[^0-9]", "");
                                if (PDER_value == "0")
                                {
                                    mbSession_DSAZ334A.RawIO.Write("*CLS");
                                    break;
                                }
                            }
                            break;
                        }
                    }
                    catch
                    {
                        PDER_value = "";
                    }
                    //ADER_value = "";
                }
                mbSession_DSAZ334A.Dispose();
            }
            catch
            {
                MessageBox.Show("WARNING : Instrument address is wrong or Instrument is shut down");
            }
        }

        private string InsertCommonEscapeSequences(string s)
        {
            return s.Replace("\n", "\\n").Replace("\r", "\\r");
        }

        private void run_Click(object sender, EventArgs e)
        {
            Log.Select();
            now_time = DateTime.Now.ToString("_MM_dd_yyyy") + DateTime.Now.ToString("_HH_mm_ss");
            if (run.Text == "Run")
            {
                BackColor = Color.FromArgb(255, 128, 128);
                thread_for_testing = new Thread(test_start);
                thread_for_testing.Start();
                run.Text = "Stop";
            }
            else
            {
                //BackColor = Color.FromArgb(128, 128, 255);
                BackColor = Color.FromArgb(255, 228, 163);
                run.Text = "Run";
                thread_for_testing.Abort();
            }
            
        }

        private void test_start()
        {
            //creat_new_ctle_excel();
            string test_lan;
            string test_freq;
            string test_port;

            //排序測試順序，根據使用者勾選的LAN、Frequence，決定循環的次數，foreach順序為LAN --> Frequence --> Port
            foreach (var lan_groupbox in Controls.OfType<GroupBox>()) 
            {
                if(lan_groupbox.Name == "group_lan")
                {
                    foreach (var lan_checkBox in lan_groupbox.Controls.OfType<CheckBox>())
                    {
                        if(lan_checkBox.Checked)
                        {
                            test_lan = lan_checkBox.Name;

                            foreach (var freq_groupbox in Controls.OfType<GroupBox>())
                            {
                                if (freq_groupbox.Name == "group_freq")
                                {
                                    foreach (var freq_checkBox in freq_groupbox.Controls.OfType<CheckBox>())
                                    {
                                        if (freq_checkBox.Checked)
                                        {
                                            test_freq = freq_checkBox.Name;

                                            foreach (var port_groupbox in Controls.OfType<GroupBox>())
                                            {
                                                if (port_groupbox.Name == "group_port")
                                                {
                                                    foreach (var port_RadioButton in port_groupbox.Controls.OfType<RadioButton>())
                                                    {
                                                        if (port_RadioButton.Checked)
                                                        {
                                                            test_port = port_RadioButton.Name;
                                                            //MessageBox.Show(test_lan + "," + test_freq + "," + test_port);
                                                            Thunderbolt_TX_test(test_lan, test_freq, test_port);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            this.Invoke((MethodInvoker)delegate ()
            {
                run.Text = "Run";
                BackColor = Color.FromArgb(255, 228, 163);
            });
        }


        void Thunderbolt_TX_test(string LAN, string freq, string port)
        {
            string Ch_ = "";
            string CIO_ = "";
            string CIO = "";
            string cio = "";
            string Dp_number = "";
            string command;
            string De = "";
            string Ps = "";
            
            if(LAN == "lan0")
            {
                Ch_ = "Ch1-3";
            }
            else if(LAN == "lan1")
            {
                Ch_ = "Ch2-4";
            }
            if (freq == "Freq10G")
            {
                CIO_ = "CIO_10G";
                CIO = "CIO_10G";
                cio = "10G";
            }
            else if (freq == "Freq10p3G")
            {
                CIO_ = "CIO_10P3G";
                CIO = "CIO_10G";
                cio = "10p3G";
            }
            else if (freq == "Freq20G")
            {
                CIO_ = "CIO_20G";
                CIO = "CIO_20G";
                cio = "20G";
            }
            else if (freq == "Freq20p6G")
            {
                CIO_ = "CIO_20P6G";
                CIO = "CIO_20G";
                cio = "20p6G";
            }
            if(tgl_mode.Checked)
            {
                Dp_number = "0";
            }           
            else
            {
                if (port == "port_a")
                {
                    Dp_number = "1";
                }
                else if (port == "port_b")
                {
                    Dp_number = "3";
                }
                else if (port == "port_c")
                {
                    Dp_number = "5";
                }
                else if (port == "port_d")
                {
                    Dp_number = "7";
                }
            }
            

            foreach (string item in TEST_FLOW.Items)
            {
                switch(item)
                {
                    case "EQ":
                        DSAZ334A(visa_name.Text, "*RST");
                        Thread.Sleep(3000);
                        DSAZ334A(visa_name.Text, ":DISK:LOAD \"C:\\USB4_SIGTEST\\" + cio + "\\" + "TP2_PRBS15_PRBS31_SQ128.set\"");
                        Check_DSAZ334A_state(visa_name.Text, ":PDER?");
                        if (freq == "Freq20p6G")
                        {
                            //command = "USB4ElectricalTestToolCLI.exe -Dp " + Dp_number + " -L0 -L1 -L All -G3 -Pa SQ128 -Tt TX -M -Pr 0 -De -Ps -D Receptacle -T";
                            command = "USB4ElectricalTestToolCLI.exe -Dp " + Dp_number + " -L0 -L1 -L All -G3 -Pa SQ128 -Tt TX -M -Pr 0 -D Receptacle -T";
                        }
                        else if (freq == "Freq20G")
                        {
                            //command = "USB4ElectricalTestToolCLI.exe -Dp " + Dp_number + " -L0 -L1 -L All -G3 -Pa SQ128 -Tt TX -M -Pr 0 -De -Ps -D Receptacle";
                            command = "USB4ElectricalTestToolCLI.exe -Dp " + Dp_number + " -L0 -L1 -L All -G3 -Pa SQ128 -Tt TX -M -Pr 0 -D Receptacle";
                        }
                        else if (freq == "Freq10p3G")
                        {
                            //command = "USB4ElectricalTestToolCLI.exe -Dp " + Dp_number + " -L0 -L1 -L All -Pa SQ128 -Tt TX -M -Pr 0 -De -Ps -D Receptacle -T";
                            command = "USB4ElectricalTestToolCLI.exe -Dp " + Dp_number + " -L0 -L1 -L All -Pa SQ128 -Tt TX -M -Pr 0 -D Receptacle -T";
                        }
                        else
                        {
                            //command = "USB4ElectricalTestToolCLI.exe -Dp " + Dp_number + " -L0 -L1 -L All -Pa SQ128 -Tt TX -M -Pr 0 -De -Ps -D Receptacle";
                            command = "USB4ElectricalTestToolCLI.exe -Dp " + Dp_number + " -L0 -L1 -L All -Pa SQ128 -Tt TX -M -Pr 0 -D Receptacle";
                        }
                        excute_cmd_tx_EQ_Psoff_Deoff(command, freq);

                        break;

                    case "EQ_Ps":
                        DSAZ334A(visa_name.Text, "*RST");
                        Thread.Sleep(3000);
                        DSAZ334A(visa_name.Text, ":DISK:LOAD \"C:\\USB4_SIGTEST\\" + cio + "\\" + "TP2_PRBS15_PRBS31_SQ128.set\"");
                        Check_DSAZ334A_state(visa_name.Text, ":PDER?");
                        if (freq == "Freq20p6G")
                        {
                            //command = "USB4ElectricalTestToolCLI.exe -Dp " + Dp_number + " -L0 -L1 -L All -G3 -Pa SQ128 -Tt TX -M -Pr 0 -De -Ps -D Receptacle -T";
                            command = "USB4ElectricalTestToolCLI.exe -Dp " + Dp_number + " -L0 -L1 -L All -G3 -Pa SQ128 -Tt TX -M -Pr 0 -D Receptacle -T";
                        }
                        else if (freq == "Freq20G")
                        {
                            //command = "USB4ElectricalTestToolCLI.exe -Dp " + Dp_number + " -L0 -L1 -L All -G3 -Pa SQ128 -Tt TX -M -Pr 0 -De -Ps -D Receptacle";
                            command = "USB4ElectricalTestToolCLI.exe -Dp " + Dp_number + " -L0 -L1 -L All -G3 -Pa SQ128 -Tt TX -M -Pr 0 -D Receptacle";
                        }
                        else if (freq == "Freq10p3G")
                        {
                            //command = "USB4ElectricalTestToolCLI.exe -Dp " + Dp_number + " -L0 -L1 -L All -Pa SQ128 -Tt TX -M -Pr 0 -De -Ps -D Receptacle -T";
                            command = "USB4ElectricalTestToolCLI.exe -Dp " + Dp_number + " -L0 -L1 -L All -Pa SQ128 -Tt TX -M -Pr 0 -D Receptacle -T";
                        }
                        else
                        {
                            command = "USB4ElectricalTestToolCLI.exe -Dp " + Dp_number + " -L0 -L1 -L All -Pa SQ128 -Tt TX -M -Pr 0 -D Receptacle";
                        }
                        excute_cmd_tx_EQ_Pson_Deoff(command, freq);

                        break;

                    case "EQ_Ps_De":
                        DSAZ334A(visa_name.Text, "*RST");
                        Thread.Sleep(3000);
                        DSAZ334A(visa_name.Text, ":DISK:LOAD \"C:\\USB4_SIGTEST\\" + cio + "\\" + "TP2_PRBS15_PRBS31_SQ128.set\"");
                        Check_DSAZ334A_state(visa_name.Text, ":PDER?");
                        if (freq == "Freq20p6G")
                        {
                            //command = "USB4ElectricalTestToolCLI.exe -Dp " + Dp_number + " -L0 -L1 -L All -G3 -Pa SQ128 -Tt TX -M -Pr 0 -De -Ps -D Receptacle -T";
                            command = "USB4ElectricalTestToolCLI.exe -Dp " + Dp_number + " -L0 -L1 -L All -G3 -Pa SQ128 -Tt TX -M -Pr 0 -D Receptacle -T";
                        }
                        else if (freq == "Freq20G")
                        {
                            //command = "USB4ElectricalTestToolCLI.exe -Dp " + Dp_number + " -L0 -L1 -L All -G3 -Pa SQ128 -Tt TX -M -Pr 0 -De -Ps -D Receptacle";
                            command = "USB4ElectricalTestToolCLI.exe -Dp " + Dp_number + " -L0 -L1 -L All -G3 -Pa SQ128 -Tt TX -M -Pr 0 -D Receptacle";
                        }
                        else if (freq == "Freq10p3G")
                        {
                            //command = "USB4ElectricalTestToolCLI.exe -Dp " + Dp_number + " -L0 -L1 -L All -Pa SQ128 -Tt TX -M -Pr 0 -De -Ps -D Receptacle -T";
                            command = "USB4ElectricalTestToolCLI.exe -Dp " + Dp_number + " -L0 -L1 -L All -Pa SQ128 -Tt TX -M -Pr 0 -D Receptacle -T";
                        }
                        else
                        {
                            //command = "USB4ElectricalTestToolCLI.exe -Dp " + Dp_number + " -L0 -L1 -L All -Pa SQ128 -Tt TX -M -Pr 0 -De -Ps -D Receptacle";
                            command = "USB4ElectricalTestToolCLI.exe -Dp " + Dp_number + " -L0 -L1 -L All -Pa SQ128 -Tt TX -M -Pr 0 -D Receptacle";
                        }
                        excute_cmd_tx_EQ_Pson_Deon(command, freq);

                        break;

                    case "EQ_De":
                        DSAZ334A(visa_name.Text, "*RST");
                        Thread.Sleep(3000);
                        DSAZ334A(visa_name.Text, ":DISK:LOAD \"C:\\USB4_SIGTEST\\" + cio + "\\" + "TP2_PRBS15_PRBS31_SQ128.set\"");
                        Check_DSAZ334A_state(visa_name.Text, ":PDER?");
                        if (freq == "Freq20p6G")
                        {
                            //command = "USB4ElectricalTestToolCLI.exe -Dp " + Dp_number + " -L0 -L1 -L All -G3 -Pa SQ128 -Tt TX -M -Pr 0 -De -Ps -D Receptacle -T";
                            command = "USB4ElectricalTestToolCLI.exe -Dp " + Dp_number + " -L0 -L1 -L All -G3 -Pa SQ128 -Tt TX -M -Pr 0 -D Receptacle -T";
                        }
                        else if (freq == "Freq20G")
                        {
                            //command = "USB4ElectricalTestToolCLI.exe -Dp " + Dp_number + " -L0 -L1 -L All -G3 -Pa SQ128 -Tt TX -M -Pr 0 -De -Ps -D Receptacle";
                            command = "USB4ElectricalTestToolCLI.exe -Dp " + Dp_number + " -L0 -L1 -L All -G3 -Pa SQ128 -Tt TX -M -Pr 0 -D Receptacle";
                        }
                        else if (freq == "Freq10p3G")
                        {
                            //command = "USB4ElectricalTestToolCLI.exe -Dp " + Dp_number + " -L0 -L1 -L All -Pa SQ128 -Tt TX -M -Pr 0 -De -Ps -D Receptacle -T";
                            command = "USB4ElectricalTestToolCLI.exe -Dp " + Dp_number + " -L0 -L1 -L All -Pa SQ128 -Tt TX -M -Pr 0 -D Receptacle -T";
                        }
                        else
                        {
                            //command = "USB4ElectricalTestToolCLI.exe -Dp " + Dp_number + " -L0 -L1 -L All -Pa SQ128 -Tt TX -M -Pr 0 -De -Ps -D Receptacle";
                            command = "USB4ElectricalTestToolCLI.exe -Dp " + Dp_number + " -L0 -L1 -L All -Pa SQ128 -Tt TX -M -Pr 0 -D Receptacle";
                        }
                        excute_cmd_tx_EQ_Psoff_Deon(command, freq);

                        break;
                    case "Preset":
                        DSAZ334A(visa_name.Text, "*RST");
                        Thread.Sleep(3000);
                        DSAZ334A(visa_name.Text, ":DISK:LOAD \"C:\\USB4_SIGTEST\\" + cio + "\\" + "TP2_PRBS15_PRBS31_SQ128.set\"");
                        Check_DSAZ334A_state(visa_name.Text, ":PDER?");
                        if (freq == "Freq20p6G")
                        {
                            command = "USB4ElectricalTestToolCLI.exe -Dp " + Dp_number + " -L0 -L1 -L All -G3 -Pa PRBS15 -Tt TX -M -Pr 0 -De -Ps -D Router -T";
                        }
                        else if (freq == "Freq20G")
                        {
                            command = "USB4ElectricalTestToolCLI.exe -Dp " + Dp_number + " -L0 -L1 -L All -G3 -Pa PRBS15 -Tt TX -M -Pr 0 -De -Ps -D Router";
                        }
                        else if (freq == "Freq10p3G")
                        {
                            command = "USB4ElectricalTestToolCLI.exe -Dp " + Dp_number + " -L0 -L1 -L All -Pa PRBS15 -Tt TX -M -Pr 0 -De -Ps -D Router -T";
                        }
                        else
                        {
                            command = "USB4ElectricalTestToolCLI.exe -Dp " + Dp_number + " -L0 -L1 -L All -Pa PRBS15 -Tt TX -M -Pr 0 -De -Ps -D Router";
                        }
                        excute_cmd_tx_preset(command, LAN, freq, port);
                        //MessageBox.Show("Preset");
                        break;
                    case "TRTF":
                        DSAZ334A(visa_name.Text, "*RST");
                        Thread.Sleep(3000);
                        DSAZ334A(visa_name.Text, ":DISK:LOAD \"C:\\USB4_SIGTEST\\" + cio + "\\" + "TP2_PRBS15_PRBS31_SQ128.set\"");
                        if (freq == "Freq20p6G")
                        {
                            command = "-Dp " + Dp_number + " -L0 -L1 -L All -G3 -Pa SQ128 -Tt TX -De -Ps -D Receptacle -T";
                        }
                        else if (freq == "Freq20G")
                        {
                            command = "-Dp " + Dp_number + " -L0 -L1 -L All -G3 -Pa SQ128 -Tt TX -De -Ps -D Receptacle";
                        }
                        else if (freq == "Freq10G")
                        {
                            command = "-Dp " + Dp_number + " -L0 -L1 -L All -Pa SQ128 -Tt TX -De -Ps -D Receptacle";
                        }
                        else
                        {
                            command = "-Dp " + Dp_number + " -L0 -L1 -L All -Pa SQ128 -Tt TX -De -Ps -D Receptacle -T";
                        }
                        USB4ElectricalTestToolCLI(command);
                        Check_DSAZ334A_state(visa_name.Text, ":PDER?");
                        DSAZ334A(visa_name.Text, ":SINGle");
                        Check_DSAZ334A_state(visa_name.Text, ":PDER?");
                        Thread.Sleep(3000);
                        if (LAN == "lan0")
                        {
                            if(port == "port_a")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"TRTF 20G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"TRTF 20G L0\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"RTFT_20G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"RTFT_20G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"TRTF 20.6G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"TRTF 20.6G L0\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"RTFT_20p6G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"RTFT_20p6G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"TRTF 10.3G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"TRTF 10.3G L0\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"RTFT_10p3G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"RTFT_10p3G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"TRTF 10G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"TRTF 10G L0\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"RTFT_10G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"RTFT_10G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                            }
                            else if (port == "port_b")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"TRTF 20G L2\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"TRTF 20G L2\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"RTFT_20G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"RTFT_20G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"TRTF 20.6G L2\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"TRTF 20.6G L2\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"RTFT_20p6G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"RTFT_20p6G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"TRTF 10.3G L2\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"TRTF 10.3G L2\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"RTFT_10p3G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"RTFT_10p3G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"TRTF 10G L2\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"TRTF 10G L2\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"RTFT_10G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"RTFT_10G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                            }
                            else if (port == "port_c")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"TRTF 20G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"TRTF 20G L0\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"RTFT_20G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"RTFT_20G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"TRTF 20.6G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"TRTF 20.6G L0\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"RTFT_20p6G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"RTFT_20p6G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"TRTF 10.3G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"TRTF 10.3G L0\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"RTFT_10p3G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"RTFT_10p3G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"TRTF 10G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"TRTF 10G L0\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"RTFT_10G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"RTFT_10G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                            }
                            else if (port == "port_d")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"TRTF 20G L2\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"TRTF 20G L2\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"RTFT_20G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"RTFT_20G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"TRTF 20.6G L2\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"TRTF 20.6G L2\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"RTFT_20p6G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"RTFT_20p6G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"TRTF 10.3G L2\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"TRTF 10.3G L2\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"RTFT_10p3G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"RTFT_10p3G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"TRTF 10G L2\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"TRTF 10G L2\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"RTFT_10G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"RTFT_10G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                            }
                        }
                        else
                        {
                            if (port == "port_a")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"TRTF 20G L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"TRTF 20G L1\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"RTFT_20G_L1\",BIN,ON");
                                    //Thread.Sleep(3000);

                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"TRTF 20.6G L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"TRTF 20.6G L1\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"RTFT_20p6G_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"TRTF 10.3G L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"TRTF 10.3G L1\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"RTFT_10p3G_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"TRTF 10G L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"TRTF 10G L1\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"RTFT_10G_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                            }
                            else if (port == "port_b")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"TRTF 20G L3\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"TRTF 20G L3\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"RTFT_20G_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"TRTF 20.6G L3\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"TRTF 20.6G L3\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"RTFT_20p6G_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"TRTF 10.3G L3\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"TRTF 10.3G L3\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"RTFT_10p3G_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"TRTF 10G L3\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"TRTF 10G L3\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"RTFT_10G_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                            }
                            else if (port == "port_c")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"TRTF 20G L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"TRTF 20G L1\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"RTFT_20G_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"TRTF 20.6G L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"TRTF 20.6G L1\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"RTFT_20p6G_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"TRTF 10.3G L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"TRTF 10.3G L1\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"RTFT_10p3G_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"TRTF 10G L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"TRTF 10G L1\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"RTFT_10G_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                            }
                            else if (port == "port_d")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"TRTF 20G L3\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"TRTF 20G L3\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"RTFT_20G_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"TRTF 20.6G L3\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"TRTF 20.6G L3\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"RTFT_20p6G_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"TRTF 10.3G L3\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"TRTF 10.3G L3\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"RTFT_10p3G_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"TRTF 10G L3\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"TRTF 10G L3\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"RTFT_10G_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                            }
                        }
                        //MessageBox.Show("TRIF");
                        break;
                    

                    case "ACCM":
                        DSAZ334A(visa_name.Text, "*RST");
                        Thread.Sleep(3000);
                        DSAZ334A(visa_name.Text, ":DISK:LOAD \"C:\\USB4_SIGTEST\\" + cio + "\\" + "TP2_CIO_TX_ACCM.set\"");
                        Check_DSAZ334A_state(visa_name.Text, ":PDER?");
                        if (freq == "Freq10p3G")
                        {
                            command = "-Dp " + Dp_number + " -L0 -L1 -L All -U Disabled -Tt TX -D Router -Pa PRBS31 -T";

                        }
                        else if(freq == "Freq10G")
                        {
                            command = "-Dp " + Dp_number + " -L0 -L1 -L All -U Disabled -Tt TX -D Router -Pa PRBS31";
                        }
                        else if (freq == "Freq20G")
                        {
                            command = "-Dp " + Dp_number + " -L0 -L1 -L All -G3 -U Disabled -Tt TX -D Router -Pa PRBS31";
                        }
                        else
                        {
                            command = "-Dp " + Dp_number + " -L0 -L1 -L All -G3 -U Disabled -Tt TX -D Router -Pa PRBS31 -T";
                        }
                        USB4ElectricalTestToolCLI(command);
                        
                        DSAZ334A(visa_name.Text, ":SINGle");
                        Check_DSAZ334A_state(visa_name.Text, ":PDER?");
                        Thread.Sleep(8000);
                        if (LAN == "lan0")
                        {
                            if (port == "port_a")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"ACCM 20G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"ACCM 20G L0\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel3,\"ACCM_20G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel4,\"ACCM_20G_L1\",BIN,ON");
                                    Thread.Sleep(3000);

                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"ACCM 20.6G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"ACCM 20.6G L0\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel3,\"ACCM_20p6G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel4,\"ACCM_20p6G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"ACCM 10.3G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"ACCM 10.3G L0\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel3,\"ACCM_10p3G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel4,\"ACCM_10p3G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"ACCM 10G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"ACCM 10G L0\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel3,\"ACCM_10G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel4,\"ACCM_10G_L1\",BIN,ON");
                                    Thread.Sleep(3000);

                                }
                            }
                            else if (port == "port_b")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"ACCM 20G L2\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"ACCM 20G L2\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel3,\"ACCM_20G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel4,\"ACCM_20G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"ACCM 20.6G L2\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"ACCM 20.6G L2\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel3,\"ACCM_20p6G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel4,\"ACCM_20p6G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"ACCM 10.3G L2\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"ACCM 10.3G L2\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel3,\"ACCM_10p3G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel4,\"ACCM_10p3G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"ACCM 10G L2\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"ACCM 10G L2\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel3,\"ACCM_10G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel4,\"ACCM_10G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                            }
                            else if (port == "port_c")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"ACCM 20G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"ACCM 20G L0\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel3,\"ACCM_20G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel4,\"ACCM_20G_L1\",BIN,ON");
                                    Thread.Sleep(3000);

                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"ACCM 20.6G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"ACCM 20.6G L0\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel3,\"ACCM_20p6G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel4,\"ACCM_20p6G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"ACCM 10.3G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"ACCM 10.3G L0\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel3,\"ACCM_10p3G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel4,\"ACCM_10p3G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"ACCM 10G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"ACCM 10G L0\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel3,\"ACCM_10G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel4,\"ACCM_10G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                            }
                            else if (port == "port_d")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"ACCM 20G L2\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"ACCM 20G L2\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel3,\"ACCM_20G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel4,\"ACCM_20G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"ACCM 20.6G L2\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"ACCM 20.6G L2\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel3,\"ACCM_20p6G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel4,\"ACCM_20p6G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"ACCM 10.3G L2\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"ACCM 10.3G L2\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel3,\"ACCM_10p3G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel4,\"ACCM_10p3G_L1\",BIN,ON");
                                    Thread.Sleep(3000);

                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"ACCM 10G L2\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"ACCM 10G L2\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel3,\"ACCM_10G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel4,\"ACCM_10G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                            }
                        }
                        else
                        {
                            if (port == "port_a")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"ACCM 20G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"ACCM 20G L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel4,\"ACCM_20G_L1\",BIN,ON");
                                    //Thread.Sleep(3000);

                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"ACCM 20.6G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"ACCM 20.6G L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel4,\"ACCM_20p6G_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"ACCM 10.3G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"ACCM 10.3G L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel4,\"ACCM_10p3G_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"ACCM 10G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"ACCM 10G L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel4,\"ACCM_10G_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                            }
                            else if (port == "port_b")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"ACCM 20G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"ACCM 20G L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel4,\"ACCM_20G_L1\",BIN,ON");
                                    //Thread.Sleep(3000);

                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"ACCM 20.6G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"ACCM 20.6G L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel4,\"ACCM_20p6G_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"ACCM 10.3G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"ACCM 10.3G L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel4,\"ACCM_10p3G_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"ACCM 10G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"ACCM 10G L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel4,\"ACCM_10G_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                            }
                            else if (port == "port_c")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"ACCM 20G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"ACCM 20G L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel4,\"ACCM_20G_L1\",BIN,ON");
                                    //Thread.Sleep(3000);

                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"ACCM 20.6G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"ACCM 20.6G L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel4,\"ACCM_20p6G_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"ACCM 10.3G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"ACCM 10.3G L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel4,\"ACCM_10p3G_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"ACCM 10G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"ACCM 10G L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel4,\"ACCM_10G_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                            }
                            else if (port == "port_d")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"ACCM 20G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"ACCM 20G L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel4,\"ACCM_20G_L1\",BIN,ON");
                                    //Thread.Sleep(3000);

                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"ACCM 20.6G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"ACCM 20.6G L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel4,\"ACCM_20p6G_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"ACCM 10.3G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"ACCM 10.3G L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel4,\"ACCM_10p3G_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"ACCM 10G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"ACCM 10G L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel4,\"ACCM_10G_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                            }
                        }

                        //MessageBox.Show("ACCM");
                        break;
                    //case "SSC": 20211122_Sam
                    //    DSAZ334A(visa_name.Text, "*RST");
                    //    Thread.Sleep(3000);
                    //    DSAZ334A(visa_name.Text, ":DISK:LOAD \"C:\\TBT4 Setup\\" + Ch_ + "\\" + CIO_ + "\\" + CIO + "_TX_SSC.set\"");
                    //    if (freq == "Freq10p3G")
                    //    {
                    //        command = "-Dp " + Dp_number + " -L0 -L1 -L All -U Disabled -Tt TX -D Router -Pa PRBS31 -T";
                    //    }
                    //    else if (freq == "Freq10G")
                    //    {
                    //        command = "-Dp " + Dp_number + " -L0 -L1 -L All -U Disabled -Tt TX -D Router -Pa PRBS31";
                    //    }
                    //    else if (freq == "Freq20p6G")
                    //    {
                    //        command = "-Dp " + Dp_number + " -L0 -L1 -L All -G3 -U Disabled -Tt TX -D Router -Pa PRBS31 -T";
                    //    }
                    //    else
                    //    {
                    //        command = "-Dp " + Dp_number + " -L0 -L1 -L All -G3 -U Disabled -Tt TX -D Router -Pa PRBS31";
                    //    }
                    //    USB4ElectricalTestToolCLI(command);
                    //    Check_DSAZ334A_state(visa_name.Text, ":ADER?");
                    //    DSAZ334A(visa_name.Text, ":SINGle");
                    //    Check_DSAZ334A_state(visa_name.Text, ":ADER?");
                    //    if (LAN == "lan0")
                    //    {
                    //        if (port == "port_a")
                    //        {
                    //            if (freq == "Freq20G")
                    //            {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"SSC 20G L0\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"SSC 20G L0\"");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"SSC 20G L0\",BIN,ON");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq20p6G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"SSC 20.6G L0\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"SSC 20.6G L0\"");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"SSC 20.6G L0\",BIN,ON");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq10p3G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"SSC 10.3G L0\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"SSC 10.3G L0\"");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"SSC 10.3G L0\",BIN,ON");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq10G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"SSC 10G L0\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"SSC 10G L0\"");
                    //                //Thread.Sleep(3000);

                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"SSC 10G L0\",BIN,ON");
                    //                Thread.Sleep(3000);
                    //            }
                    //        }
                    //        else if (port == "port_b")
                    //        {
                    //            if (freq == "Freq20G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"SSC 20G L2\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"SSC 20G L2\"");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"SSC 20G L2\",BIN,ON");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq20p6G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"SSC 20.6G L2\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"SSC 20.6G L2\"");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"SSC 20.6G L2\",BIN,ON");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq10p3G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"SSC 10.3G L2\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"SSC 10.3G L2\"");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"SSC 10.3G L2\",BIN,ON");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq10G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"SSC 10G L2\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"SSC 10G L2\"");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"SSC 10G L2\",BIN,ON");
                    //                Thread.Sleep(3000);
                    //            }
                    //        }
                    //        else if (port == "port_c")
                    //        {
                    //            if (freq == "Freq20G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"SSC 20G L0\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"SSC 20G L0\"");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"SSC 20G L0\",BIN,ON");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq20p6G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"SSC 20.6G L0\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"SSC 20.6G L0\"");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"SSC 20.6G L0\",BIN,ON");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq10p3G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"SSC 10.3G L0\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"SSC 10.3G L0\"");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"SSC 10.3G L0\",BIN,ON");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq10G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"SSC 10G L0\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"SSC 10G L0\"");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"SSC 10G L0\",BIN,ON");
                    //                Thread.Sleep(3000);
                    //            }
                    //        }
                    //        else if (port == "port_d")
                    //        {
                    //            if (freq == "Freq20G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"SSC 20G L2\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"SSC 20G L2\"");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"SSC 20G L2\",BIN,ON");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq20p6G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"SSC 20.6G L2\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"SSC 20.6G L2\"");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"SSC 20.6G L2\",BIN,ON");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq10p3G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"SSC 10.3G L2\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"SSC 10.3G L2\"");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"SSC 10.3G L2\",BIN,ON");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq10G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"SSC 10G L2\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"SSC 10G L2\"");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"SSC 10G L2\",BIN,ON");
                    //                Thread.Sleep(3000);
                    //            }
                    //        }
                    //    }
                    //    else
                    //    {
                    //        if (port == "port_a")
                    //        {
                    //            if (freq == "Freq20G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"SSC 20G L1\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"SSC 20G L1\"");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"SSC 20G L1\",BIN,ON");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq20p6G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"SSC 20.6G L1\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"SSC 20.6G L1\"");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"SSC 20.6G L1\",BIN,ON");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq10p3G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"SSC 10.3G L1\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"SSC 10.3G L1\"");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"SSC 10.3G L1\",BIN,ON");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq10G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"SSC 10G L1\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"SSC 10G L1\"");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"SSC 10G L1\",BIN,ON");
                    //                Thread.Sleep(3000);
                    //            }
                    //        }
                    //        else if (port == "port_b")
                    //        {
                    //            if (freq == "Freq20G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"SSC 20G L3\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"SSC 20G L3\"");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"SSC 20G L3\",BIN,ON");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq20p6G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"SSC 20.6G L3\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"SSC 20.6G L3\"");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"SSC 20.6G L3\",BIN,ON");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq10p3G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"SSC 10.3G L3\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"SSC 10.3G L3\"");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"SSC 10.3G L3\",BIN,ON");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq10G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"SSC 10G L3\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"SSC 10G L3\"");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"SSC 10G L3\",BIN,ON");
                    //                Thread.Sleep(3000);
                    //            }
                    //        }
                    //        else if (port == "port_c")
                    //        {
                    //            if (freq == "Freq20G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"SSC 20G L1\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"SSC 20G L1\"");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"SSC 20G L1\",BIN,ON");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq20p6G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"SSC 20.6G L1\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"SSC 20.6G L1\"");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"SSC 20.6G L1\",BIN,ON");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq10p3G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"SSC 10.3G L1\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"SSC 10.3G L1\"");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"SSC 10.3G L1\",BIN,ON");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq10G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"SSC 10G L1\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"SSC 10G L1\"");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"SSC 10G L1\",BIN,ON");
                    //                Thread.Sleep(3000);
                    //            }
                    //        }
                    //        else if (port == "port_d")
                    //        {
                    //            if (freq == "Freq20G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"SSC 20G L3\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"SSC 20G L3\"");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"SSC 20G L3\",BIN,ON");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq20p6G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"SSC 20.6G L3\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"SSC 20.6G L3\"");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"SSC 20.6G L3\",BIN,ON");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq10p3G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"SSC 10.3G L3\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"SSC 10.3G L3\"");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"SSC 10.3G L3\",BIN,ON");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq10G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"SSC 10G L3\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:MEASurements \"SSC 10G L3\"");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"SSC 10G L3\",BIN,ON");
                    //                Thread.Sleep(3000);
                    //            }
                    //        }
                    //    }
                    //    //MessageBox.Show("SSC");
                    //    break;

                    case "EYE"://TP2_xxG_EYE_L0/1  20211122_Sam+
                        DSAZ334A(visa_name.Text, "*RST");
                        Thread.Sleep(3000);
                        DSAZ334A_isrun = 0;
                        DSAZ334A(visa_name.Text, ":DISK:LOAD \"C:\\USB4_SIGTEST\\" + cio + "\\" + "TP2_PRBS15_PRBS31_SQ128.set\"");
                        if (freq == "Freq10p3G")
                        {
                            command = "-Dp " + Dp_number + " -L0 -L1 -L All -U Disabled -Tt TX -D Router -Pa PRBS31 -T";
                        }
                        else if (freq == "Freq10G")
                        {
                            command = "-Dp " + Dp_number + " -L0 -L1 -L All -U Disabled -Tt TX -D Router -Pa PRBS31";
                        }
                        else if (freq == "Freq20G")
                        {
                            command = "-Dp " + Dp_number + " -L0 -L1 -L All -G3 -U Disabled -Tt TX -D Router -Pa PRBS31";
                        }
                        else
                        {
                            command = "-Dp " + Dp_number + " -L0 -L1 -L All -G3 -U Disabled -Tt TX -D Router -Pa PRBS31 -T";
                        }
                        USB4ElectricalTestToolCLI(command);
                        Check_DSAZ334A_state(visa_name.Text, ":ADER?");
                        //DSAZ334A(visa_name.Text, ":MTESt:STARt");
                        DSAZ334A(visa_name.Text, ":SINGle");
                        Check_DSAZ334A_state(visa_name.Text, ":ADER?");
                        if (LAN == "lan0")
                        {
                            if (port == "port_a")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 20G TP2 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"TP2_20G_EYE_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_20G_EYE_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 20.6G TP2 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"TP2_20p6G_EYE_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_20p6G_EYE_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 10.3G TP2 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"TP2_10p3G_EYE_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_10p3G_EYE_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 10G TP2 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"TP2_10G_EYE_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_10G_EYE_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                            }
                            else if (port == "port_b")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 20G TP2 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"TP2_20G_EYE_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_20G_EYE_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 20.6G TP2 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"TP2_20p6G_EYE_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_20p6G_EYE_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 10.3G TP2 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"TP2_10p3G_EYE_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_10p3G_EYE_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 10G TP2 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"TP2_10G_EYE_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_10G_EYE_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                            }
                            else if (port == "port_c")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 20G TP2 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"TP2_20G_EYE_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_20G_EYE_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 20.6G TP2 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"TP2_20p6G_EYE_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_20p6G_EYE_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 10.3G TP2 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"TP2_10p3G_EYE_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_10p3G_EYE_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 10G TP2 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"TP2_10G_EYE_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_10G_EYE_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                            }
                            else if (port == "port_d")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 20G TP2 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"TP2_20G_EYE_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_20G_EYE_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 20.6G TP2 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"TP2_20p6G_EYE_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_20p6G_EYE_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 10.3G TP2 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"TP2_10p3G_EYE_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_10p3G_EYE_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 10G TP2 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"TP2_10G_EYE_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_10G_EYE_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                            }
                        }
                        else
                        {
                            if (port == "port_a")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 20G TP2 L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_20G_EYE_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 20.6G TP2 L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_20p6G EYE_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 10.3G TP2 L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_10p3G_EYE_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 10G TP2 L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_10G_EYE_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                            }
                            else if (port == "port_b")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 20G TP2 L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_20G_EYE_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 20.6G TP2 L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_20p6G EYE_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 10.3G TP2 L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_10p3G_EYE_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 10G TP2 L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_10G_EYE_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                            }
                            else if (port == "port_c")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 20G TP2 L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_20G_EYE_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 20.6G TP2 L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_20p6G EYE_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 10.3G TP2 L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_10p3G_EYE_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 10G TP2 L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_10G_EYE_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                            }
                            else if (port == "port_d")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 20G TP2 L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_20G_EYE_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 20.6G TP2 L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_20p6G EYE_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 10.3G TP2 L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_10p3G_EYE_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 10G TP2 L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_10G_EYE_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                            }
                        }
                        //MessageBox.Show("EYE");
                        break;

                    case "Jitter": //TP2_xxG_JITTER_L0/1 20211122_Sam
                        DSAZ334A(visa_name.Text, "*RST");
                        Thread.Sleep(3000);
                        DSAZ334A(visa_name.Text, ":DISK:LOAD \"C:\\USB4_SIGTEST\\" + cio + "\\" + "TP2_PRBS15_PRBS31_SQ128.set\"");
                        if (freq == "Freq10p3G")
                        {
                            command = "-Dp " + Dp_number + " -L0 -L1 -L All -U Disabled -Tt TX -D Router -Pa PRBS15 -T";
                        }
                        else if (freq =="Freq10G")
                        {
                            command = "-Dp " + Dp_number + " -L0 -L1 -L All -U Disabled -Tt TX -D Router -Pa PRBS15";
                        }
                        else if (freq == "Freq20G")
                        {
                            command = "-Dp " + Dp_number + " -L0 -L1 -L All -G3 -U Disabled -Tt TX -D Router -Pa PRBS15";
                        }
                        else
                        {
                            command = "-Dp " + Dp_number + " -L0 -L1 -L All -G3 -U Disabled -Tt TX -D Router -Pa PRBS15 -T";
                        }
                        USB4ElectricalTestToolCLI(command);
                        Check_DSAZ334A_state(visa_name.Text, ":PDER?");
                        DSAZ334A(visa_name.Text, ":SINGle");
                        Check_DSAZ334A_state(visa_name.Text, ":PDER?");
                        if (LAN == "lan0")
                        {
                            if (port == "port_a")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 20G TP2 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 20G TP2 L0\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"TP2_20G_Jitter_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_20G_Jitter_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 20.6G TP2 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 20.6G TP2 L0\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"TP2_20p6G_Jitter_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_20p6G_Jitter_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 10.3G TP2 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 10.3G TP2 L0\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"TP2_10p3G_Jitter_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_10p3G_Jitter_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 10G TP2 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 10G TP2 L0\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"TP2_10G_Jitter_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_10G_Jitter_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                            }
                            else if (port == "port_b")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 20G TP2 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 20G TP2 L0\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"TP2_20G_Jitter_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_20G_Jitter_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 20.6G TP2 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 20.6G TP2 L0\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"TP2_20p6G_Jitter_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_20p6G_Jitter_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 10.3G TP2 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 10.3G TP2 L0\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"TP2_10p3G_Jitter_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_10p3G_Jitter_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 10G TP2 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 10G TP2 L0\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"TP2_10G_Jitter_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_10G_Jitter_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                            }
                            else if (port == "port_c")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 20G TP2 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 20G TP2 L0\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"TP2_20G_Jitter_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_20G_Jitter_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 20.6G TP2 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 20.6G TP2 L0\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"TP2_20p6G_Jitter_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_20p6G_Jitter_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 10.3G TP2 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 10.3G TP2 L0\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"TP2_10p3G_Jitter_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_10p3G_Jitter_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 10G TP2 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 10G TP2 L0\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"TP2_10G_Jitter_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_10G_Jitter_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                            }
                            else if (port == "port_d")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 20G TP2 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 20G TP2 L0\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"TP2_20G_Jitter_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_20G_Jitter_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 20.6G TP2 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 20.6G TP2 L0\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"TP2_20p6G_Jitter_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_20p6G_Jitter_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 10.3G TP2 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 10.3G TP2 L0\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"TP2_10p3G_Jitter_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_10p3G_Jitter_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 10G TP2 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 10G TP2 L0\"");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"TP2_10G_Jitter_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_10G_Jitter_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                            }
                        }
                        else
                        {
                            if (port == "port_a")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 20G TP2 L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 20G TP2 L1\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_20G_Jitter_L1\",BIN,ON");
                                    //Thread.Sleep(3000);

                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 20.6G TP2 L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 20.6G TP2 L1\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_20p6G_Jitter_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 10.3G TP2 L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 10.3G TP2 L1\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_10p3G_Jitter_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 10G TP2 L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 10G TP2 L1\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_10G_Jitter_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                            }
                            else if (port == "port_b")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 20G TP2 L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 20G TP2 L1\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_20G_Jitter_L1\",BIN,ON");
                                    //Thread.Sleep(3000);

                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 20.6G TP2 L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 20.6G TP2 L1\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_20p6G_Jitter_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 10.3G TP2 L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 10.3G TP2 L1\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_10p3G_Jitter_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 10G TP2 L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 10G TP2 L1\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_10G_Jitter_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                            }
                            else if (port == "port_c")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 20G TP2 L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 20G TP2 L1\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_20G_Jitter_L1\",BIN,ON");
                                    //Thread.Sleep(3000);

                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 20.6G TP2 L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 20.6G TP2 L1\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_20p6G_Jitter_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 10.3G TP2 L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 10.3G TP2 L1\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_10p3G_Jitter_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 10G TP2 L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 10G TP2 L1\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_10G_Jitter_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                            }
                            else if (port == "port_d")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 20G TP2 L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 20G TP2 L1\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_20G_Jitter_L1\",BIN,ON");
                                    //Thread.Sleep(3000);

                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 20.6G TP2 L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 20.6G TP2 L1\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_20p6G_Jitter_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 10.3G TP2 L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 10.3G TP2 L1\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_10p3G_Jitter_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 10G TP2 L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 10G TP2 L1\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"TP2_10G_Jitter_L1\",BIN,ON");
                                    //Thread.Sleep(3000);
                                }
                            }
                        }
                        //MessageBox.Show("Jitter");
                        break;

                    //case "LF UDJ": 20211122_Sam
                    //    DSAZ334A(visa_name.Text, "*RST");
                    //    Thread.Sleep(3000);
                    //    DSAZ334A(visa_name.Text, ":DISK:LOAD \"C:\\TBT4 Setup\\" + Ch_ + "\\" + CIO_ + "\\" + CIO + "_TX_UDJ_LF.set\"");
                    //    if (freq == "Freq10p3G")
                    //    {
                    //        command = "-Dp " + Dp_number + " -L0 -L1 -L All -U Disabled -Tt TX -D Router -Pa PRBS15 -T";
                    //    }
                    //    else if (freq == "Freq10G")
                    //    {
                    //        command = "-Dp " + Dp_number + " -L0 -L1 -L All -U Disabled -Tt TX -D Router -Pa PRBS15";
                    //    }
                    //    else if (freq == "Freq20p6G")
                    //    {
                    //        command = "-Dp " + Dp_number + " -L0 -L1 -L All -G3 -U Disabled -Tt TX -D Router -Pa PRBS15 -T";
                    //    }
                    //    else
                    //    {
                    //        command = "-Dp " + Dp_number + " -L0 -L1 -L All-G3 -U Disabled -Tt TX -D Router -Pa PRBS15 ";
                    //    }
                    //    USB4ElectricalTestToolCLI(command);
                    //    Check_DSAZ334A_state(visa_name.Text, ":PDER?");
                    //    DSAZ334A(visa_name.Text, ":SINGle");
                    //    Check_DSAZ334A_state(visa_name.Text, ":PDER?");
                    //    if (LAN == "lan0")
                    //    {
                    //        if (port == "port_a")
                    //        {
                    //            if (freq == "Freq20G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"LF Jitter 20G TP2 L0\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"LF Jitter 20G TP2 L0\"");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq20p6G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"LF Jitter 20.6G TP2 L0\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"LF Jitter 20.6G TP2 L0\"");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq10p3G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"LF Jitter 10.3G TP2 L0\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"LF Jitter 10.3G TP2 L0\"");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq10G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"LF Jitter 10G TP2 L0\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"LF Jitter 10G TP2 L0\"");
                    //                Thread.Sleep(3000);
                    //            }
                    //        }
                    //        else if (port == "port_b")
                    //        {
                    //            if (freq == "Freq20G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"LF Jitter 20G TP2 L2\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"LF Jitter 20G TP2 L2\"");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq20p6G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"LF Jitter 20.6G TP2 L2\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"LF Jitter 20.6G TP2 L2\"");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq10p3G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"LF Jitter 10.3G TP2 L2\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"LF Jitter 10.3G TP2 L2\"");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq10G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"LF Jitter 10G TP2 L2\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"LF Jitter 10G TP2 L2\"");
                    //                Thread.Sleep(3000);
                    //            }
                    //        }
                    //        else if (port == "port_c")
                    //        {
                    //            if (freq == "Freq20G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"LF Jitter 20G TP2 L0\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"LF Jitter 20G TP2 L0\"");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq20p6G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"LF Jitter 20.6G TP2 L0\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"LF Jitter 20.6G TP2 L0\"");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq10p3G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"LF Jitter 10.3G TP2 L0\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"LF Jitter 10.3G TP2 L0\"");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq10G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"LF Jitter 10G TP2 L0\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"LF Jitter 10G TP2 L0\"");
                    //                Thread.Sleep(3000);
                    //            }
                    //        }
                    //        else if (port == "port_d")
                    //        {
                    //            if (freq == "Freq20G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"LF Jitter 20G TP2 L2\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"LF Jitter 20G TP2 L2\"");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq20p6G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"LF Jitter 20.6G TP2 L2\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"LF Jitter 20.6G TP2 L2\"");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq10p3G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"LF Jitter 10.3G L2\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"LF Jitter 10.3G TP2 L2\"");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq10G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"LF Jitter 10G TP2 L2\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"LF Jitter 10G TP2 L2\"");
                    //                Thread.Sleep(3000);
                    //            }
                    //        }
                    //    }
                    //    //Thread.Sleep(60000);
                    //    else
                    //    {
                    //        if (port == "port_a")
                    //        {
                    //            if (freq == "Freq20G")
                    //            {
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"LF Jitter 20G TP2 L1\",PNG,SCR,ON,NORMal");
                    //                Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"LF Jitter 20G TP2 L1\"");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq20p6G")
                    //            {
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"LF Jitter 20.6G TP2 L1\",PNG,SCR,ON,NORMal");
                    //                Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"LF Jitter 20.6G TP2 L1\"");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq10p3G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"LF Jitter 10.3G TP2 L1\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"LF Jitter 10.3G TP2 L1\"");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq10G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"LF Jitter 10G TP2 L1\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"LF Jitter 10G TP2 L1\"");
                    //                Thread.Sleep(3000);
                    //            }
                    //        }
                    //        else if (port == "port_b")
                    //        {
                    //            if (freq == "Freq20G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"LF Jitter 20G TP2 L3\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"LF Jitter 20G TP2 L3\"");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq20p6G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"LF Jitter 20.6G TP2 L3\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"LF Jitter 20.6G TP2 L3\"");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq10p3G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"LF Jitter 10.3G TP2 L3\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"LF Jitter 10.3G TP2 L3\"");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq10G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"LF Jitter 10G TP2 L3\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"LF Jitter 10G TP2 L3\"");
                    //                Thread.Sleep(3000);
                    //            }
                    //        }
                    //        else if (port == "port_c")
                    //        {
                    //            if (freq == "Freq20G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"LF Jitter 20G TP2 L1\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"LF Jitter 20G TP2 L1\"");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq20p6G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"LF Jitter 20.6G TP2 L1\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"LF Jitter 20.6G TP2 L1\"");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq10p3G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"LF Jitter 10.3G TP2 L1\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"LF Jitter 10.3G TP2 L1\"");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq10G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"LF Jitter 10G TP2 L1\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"LF Jitter 10G TP2 L1\"");
                    //                Thread.Sleep(3000);
                    //            }
                    //        }
                    //        else if (port == "port_d")
                    //        {
                    //            if (freq == "Freq20G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"LF Jitter 20G TP2 L3\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"LF Jitter 20G TP2 L3\"");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq20p6G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"LF Jitter 20.6G TP2 L3\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"LF Jitter 20.6G TP2 L3\"");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq10p3G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"LF Jitter 10.3G TP2 L3\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"LF Jitter 10.3G TP2 L3\"");
                    //                Thread.Sleep(3000);
                    //            }
                    //            else if (freq == "Freq10G")
                    //            {
                    //                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"LF Jitter 10G TP2 L3\",PNG,SCR,ON,NORMal");
                    //                //Thread.Sleep(3000);
                    //                DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"LF Jitter 10G TP2 L3\"");
                    //                Thread.Sleep(3000);
                    //            }
                    //        }
                    //    }
                    //    //MessageBox.Show("LF UDJ");
                    //    break;

                    case "EIDLE":
                        DSAZ334A(visa_name.Text, "*RST");
                        Thread.Sleep(3000);
                        DSAZ334A(visa_name.Text, ":DISK:LOAD \"C:\\USB4_SIGTEST\\" + cio + "\\" + "TP2_CIO_TX_Eidle.set\"");
                        if (freq == "Freq10p3G")
                        {
                            if (LAN =="lan0")
                            {
                                command = "-Dp " + Dp_number + " -L0 -L1 -L 0 -T -Tt ENTER_EI_TEST -D All -T";
                            }
                            else
                            {
                                command = "-Dp " + Dp_number + " -L0 -L1 -L 1 -T -Tt ENTER_EI_TEST -D All -T";
                            }
                        }
                        else if (freq == "Freq10G")
                        {
                            if (LAN == "lan0")
                            {
                                command = "-Dp " + Dp_number + " -L0 -L1 -L 0 -T -Tt ENTER_EI_TEST -D All";
                            }
                            else
                            {
                                command = "-Dp " + Dp_number + " -L0 -L1 -L 1 -T -Tt ENTER_EI_TEST -D All";
                            }
                        }
                        else if (freq == "Freq20p6G")
                        {
                            if (LAN == "lan0")
                            {
                                command = "-Dp " + Dp_number + " -L0 -L1 -L 0 -G3 -Tt ENTER_EI_TEST -D All -Sw None -T";
                            }
                            else
                            {
                                command = "-Dp " + Dp_number + " -L0 -L1 -L 1 -G3 -Tt ENTER_EI_TEST -D All -Sw None -T";
                            }
                        }
                        else
                        {
                            if (LAN == "lan0")
                            {
                                command = "-Dp " + Dp_number + " -L0 -L0 -L 0 -G3 -Tt ENTER_EI_TEST -D All -Sw None";
                            }
                            else
                            {
                                command = "-Dp " + Dp_number + " -L0 -L1 -L 1 -G3 -Tt ENTER_EI_TEST -D All -Sw None";
                            }
                        }
                        USB4ElectricalTestToolCLI(command);
                        Check_DSAZ334A_state(visa_name.Text, ":ADER?");
                        DSAZ334A(visa_name.Text, ":SINGle");
                        Check_DSAZ334A_state(visa_name.Text, ":ADER?");
                        Thread.Sleep(1000);
                        if (LAN == "lan0")
                        {
                            if (port == "port_a")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EIDLE 20G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"EIDLE_20G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EIDLE 20.6G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"EIDLE_20p6G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EIDLE 10.3G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"EIDLE_10p3G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EIDLE 10G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"EIDLE_10G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                            }
                            else if (port == "port_b")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EIDLE 20G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"EIDLE_20G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EIDLE 20.6G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"EIDLE_20p6G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EIDLE 10.3G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"EIDLE_10p3G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EIDLE 10G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"EIDLE_10G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                            }
                            else if (port == "port_c")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EIDLE 20G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"EIDLE_20G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EIDLE 20.6G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"EIDLE_20p6G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EIDLE 10.3G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"EIDLE_10p3G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EIDLE 10G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"EIDLE_10G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                            }
                            else if (port == "port_d")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EIDLE 20G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"EIDLE_20G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EIDLE 20.6G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"EIDLE_20p6G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EIDLE 10.3G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"EIDLE_10p3G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EIDLE 10G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"EIDLE_10G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                            }
                        }
                        else
                        {
                            if (port == "port_a")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EIDLE 20G L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"EIDLE_20G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EIDLE 20.6G L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"EIDLE_20p6G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EIDLE 10.3G L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"EIDLE_10p3G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EIDLE 10G L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"EIDLE_10G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                            }
                            else if (port == "port_b")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EIDLE 20G L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"EIDLE_20G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EIDLE 20.6G L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"EIDLE_20p6G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EIDLE 10.3G L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"EIDLE_10p3G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EIDLE 10G L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"EIDLE_10G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                            }
                            else if (port == "port_c")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EIDLE 20G L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"EIDLE_20G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EIDLE 20.6G L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"EIDLE_20p6G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EIDLE 10.3G L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"EIDLE_10p3G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EIDLE 10G L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"EIDLE_10G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                            }
                            else if (port == "port_d")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EIDLE 20G L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"EIDLE_20G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EIDLE 20.6G L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"EIDLE_20p6G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EIDLE 10.3G L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"EIDLE_10p3G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EIDLE 10G L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"EIDLE_10G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                            }
                        }
                        //MessageBox.Show("EIDLE");
                        break;

                    case "CLK_SW":
                        //Log.Select();
                        DSAZ334A(visa_name.Text, "*RST");
                        Thread.Sleep(3000);
                        if (cio == "10G")
                        {
                            if (LAN == "lan0")
                            {
                                DSAZ334A(visa_name.Text, ":DISK:LOAD \"C:\\USB4_SIGTEST\\" + cio + "\\" + "CIO_TX_CLK_SW_CH1-3.set\"");
                            }
                            else
                            {
                                DSAZ334A(visa_name.Text, ":DISK:LOAD \"C:\\USB4_SIGTEST\\" + cio + "\\" + "CIO_TX_CLK_SW_CH2-4.set\"");
                            }
                            
                        }
                        else if (cio == "10p3G")
                        {
                            if (LAN == "lan0")
                            {
                                DSAZ334A(visa_name.Text, ":DISK:LOAD \"C:\\USB4_SIGTEST\\" + cio + "\\" + "CIO_TX_CLK_SW_CH1-3.set\"");
                            }
                            else
                            {
                                DSAZ334A(visa_name.Text, ":DISK:LOAD \"C:\\USB4_SIGTEST\\" + cio + "\\" + "CIO_TX_CLK_SW_CH2-4.set\"");
                            }
                        }
                        else if (cio == "20G")
                        {
                            if (LAN == "lan0")
                            {
                                DSAZ334A(visa_name.Text, ":DISK:LOAD \"C:\\USB4_SIGTEST\\" + cio + "\\" + "CIO_TX_CLK_SW_CH1-3.set\"");
                            }
                            else
                            {
                                DSAZ334A(visa_name.Text, ":DISK:LOAD \"C:\\USB4_SIGTEST\\" + cio + "\\" + "CIO_TX_CLK_SW_CH2-4.set\"");
                            }
                        }
                        else if (cio == "20p6G")
                        {
                            if (LAN == "lan0")
                            {
                                DSAZ334A(visa_name.Text, ":DISK:LOAD \"C:\\USB4_SIGTEST\\" + cio + "\\" + "CIO_TX_CLK_SW_CH1-3.set\"");
                            }
                            else
                            {
                                DSAZ334A(visa_name.Text, ":DISK:LOAD \"C:\\USB4_SIGTEST\\" + cio + "\\" + "CIO_TX_CLK_SW_CH2-4.set\"");
                            }
                        }
                            
                        Check_DSAZ334A_state(visa_name.Text, ":PDER?");
                        if (freq == "Freq20p6G")
                        {
                            excute_cmd_tx_CLK("-Dp " + Dp_number + " -L0  -L1  -L All -Tt TXFREQVARTRAIN -D TxFrequencyVariation -Sw Router -Pa PRBS31 -G3 -T");
                        }
                        else if(freq == "Freq20G")
                        {
                            excute_cmd_tx_CLK("-Dp " + Dp_number + " -L0  -L1  -L All -Tt TXFREQVARTRAIN -D TxFrequencyVariation -Sw Router -Pa PRBS31 -G3");
                        }
                        else if (freq == "Freq10p3G")
                        {
                            excute_cmd_tx_CLK("-Dp " + Dp_number + " -L0  -L1  -L All -Tt TXFREQVARTRAIN -D TxFrequencyVariation -Sw Router -Pa PRBS31 -T");
                        }
                        else
                        {
                            excute_cmd_tx_CLK("-Dp " + Dp_number + " -L0  -L1  -L All -Tt TXFREQVARTRAIN -D TxFrequencyVariation -Sw Router -Pa PRBS31");
                        }
                        //DSAZ334A(visa_name.Text, ":SINGle");
                        //Check_DSAZ334A_state(visa_name.Text, ":PDER?");
                        if (LAN == "lan0")
                        {
                            if (port == "port_a")
                            {

                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"CLK_SW_20G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"CLK_SW_20G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                   
                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"CLK_SW_20.6G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"CLK_SW_20p6G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"CLK_SW_10.3G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"CLK_SW_10p3G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"CLK_SW_10G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"CLK_SW_10G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    
                                }
                            }
                            else if (port == "port_b")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"CLK_SW_20G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"CLK_SW_20G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    
                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"CLK_SW_20.6G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"CLK_SW_20p6G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"CLK_SW_10.3G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"CLK_SW_10p3G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"CLK_SW_10G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"CLK_SW_10G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                   
                                }
                            }
                            else if (port == "port_c")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"CLK_SW_20G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"CLK_SW_20G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                   
                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"CLK_SW_20.6G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"CLK_SW_20p6G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                   
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"CLK_SW_10.3G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"CLK_SW_10p3G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                   
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"CLK_SW_10G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"CLK_SW_10G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                    
                                }
                            }
                            else if (port == "port_d")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"CLK_SW_20G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"CLK_SW_20G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                   
                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"CLK_SW_20.6G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"CLK_SW_20p6G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                  
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"CLK_SW_10.3G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"CLK_SW_10p3G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                   
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"CLK_SW_10G L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"CLK_SW_10G_L0\",BIN,ON");
                                    Thread.Sleep(3000);
                                   
                                }
                            }
                        }
                        else
                        {
                            if (port == "port_a")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"CLK_SW_20G L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"CLK_SW_20G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"CLK_SW_20.6G L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"CLK_SW_20p6G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"CLK_SW_10.3G L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"CLK_SW_10p3G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"CLK_SW_10G L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"CLK_SW_10G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                            }
                            else if (port == "port_b")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"CLK_SW_20G L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"CLK_SW_20G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"CLK_SW_20.6G L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"CLK_SW_20p6G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"CLK_SW_10.3G L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"CLK_SW_10p3G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"CLK_SW_10G L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"CLK_SW_10G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                            }
                            else if (port == "port_c")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"CLK_SW_20G L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"CLK_SW_20G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"CLK_SW_20.6G L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"CLK_SW_20p6G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"CLK_SW_10.3G L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"CLK_SW_10p3G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"CLK_SW_10G L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"CLK_SW_10G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                            }
                            else if (port == "port_d")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"CLK_SW_20G L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"CLK_SW_20G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"CLK_SW_20.6G L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"CLK_SW_20p6G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"CLK_SW_10.3G L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"CLK_SW_10p3G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"CLK_SW_10G L1\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"CLK_SW_10G_L1\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                            }
                        }
                        //MessageBox.Show("CLK_SW");
                        break;

                    case "EYE_Passive":
                        DSAZ334A_query_value = "";
                        DSAZ334A_isrun = 0;
                        //int pos1;
                        //int pos2;
                        //int pos3;
                        //int pos4;
                        //double g;
                        //string[] gain = { "0.891", "0.794", "0.708", "0.631", "0.562", "0.501", "0.447" };
                        //double[] array = new double[8];
                        //double[] width = new double[8];
                        //double[] height = new double[8];

                        //creat_new_ctle_excel(LAN);
                        DSAZ334A(visa_name.Text, "*RST");
                        Thread.Sleep(3000);
                        DSAZ334A(visa_name.Text, ":DISK:LOAD \"C:\\USB4_SIGTEST\\" + cio + "\\" + "TP3_PRBS15_5times_PRBS31.set\"");
                        Check_DSAZ334A_state(visa_name.Text, ":PDER?");

                        if (freq == "Freq10p3G")
                        {
                            command = ("-Dp " + Dp_number + " -L0 -L1 -L All -U Disabled -Tt TX -D Router -Pa PRBS31 -T");
                        }
                        else if (freq == "Freq10G")
                        {
                            command = ("-Dp " + Dp_number + " -L0 -L1 -L All -U Disabled -Tt TX -D Router -Pa PRBS31");
                        }
                        else if (freq == "Freq20p6G")
                        {
                            command = ("-Dp " + Dp_number + " -L0 -L1 -L All -U Disabled -Tt TX -D Router -Pa PRBS31 -G3 -T");
                        }
                        else
                        {
                            command = ("-Dp " + Dp_number + " -L0 -L1 -L All -U Disabled -Tt TX -D Router -Pa PRBS31 -G3");
                        }
                        //MessageBox.Show("setup finish");
                        USB4ElectricalTestToolCLI(command);
                        //DSAZ334A(visa_name.Text, "*RST");
                        //Check_DSAZ334A_state(visa_name.Text, ":PDER?");
                        //DSAZ334A(visa_name.Text, ":SINGle");
                        //Check_DSAZ334A_state(visa_name.Text, ":PDER?");


                        //MessageBox.Show("2222");
                        //Check_DSAZ334A_state(visa_name.Text, ":ADER?");



                        //    DSAZ334A_query(visa_name.Text, ":MEASure:RESults?"); //,shawn,2020.11.11
                        //                                                         //Check_DSAZ334A_state(visa_name.Text, ":PDER?");
                        //    DSAZ334A_query_value = DSAZ334A_query_value.Replace(",9.99999E+37", "");
                        //    pos1 = DSAZ334A_query_value.IndexOf("width");
                        //    pos2 = DSAZ334A_query_value.IndexOf(",", pos1);
                        //    pos3 = DSAZ334A_query_value.IndexOf("E", pos1);
                        //    pos4 = DSAZ334A_query_value.IndexOf(",", pos3);
                        //    g = Convert.ToDouble(DSAZ334A_query_value.Substring(pos3 + 1, pos4 - pos3));
                        //    g = Math.Pow(10, g) * Math.Pow(10, 11);
                        //    width[k] = Math.Round(Convert.ToDouble(DSAZ334A_query_value.Substring(pos2 + 1, pos3 - pos2 - 1)) * g, 3);

                        //    pos1 = DSAZ334A_query_value.IndexOf("height");
                        //    pos2 = DSAZ334A_query_value.IndexOf(",", pos1);
                        //    pos3 = DSAZ334A_query_value.IndexOf("E", pos1);
                        //    pos4 = DSAZ334A_query_value.IndexOf(",", pos3);
                        //    g = Convert.ToDouble(DSAZ334A_query_value.Substring(pos3 + 1, pos4 - pos3));
                        //    g = Math.Pow(10, g) * Math.Pow(10, 1);
                        //    height[k] = Math.Round(Convert.ToDouble(DSAZ334A_query_value.Substring(pos2 + 1, pos3 - pos2 - 1)) * g, 3);


                        //    for (int j = 0; j < 7; j++)
                        //{
                        //    DSAZ334A(visa_name.Text, ":CDISplay");
                        //    DSAZ334A(visa_name.Text, ":LANE1:EQUalizer:CTLE:DCGain " + gain[j]);
                        //    Check_DSAZ334A_state(visa_name.Text, ":PDER?");
                        //    Thread.Sleep(2000);
                        //    DSAZ334A(visa_name.Text, ":SINGle");

                        //    //Check_DSAZ334AOLD_state(visa_name.Text, ":ADER?");
                        //    Thread.Sleep(2000);
                        //    while (true) //檢查run是否完成
                        //    {
                        //        Check_DSAZ334AOLD_state(visa_name.Text, ":ADER?");
                        //        Thread.Sleep(100);
                        //        if (PDER_value == "" || PDER_value == "0")
                        //        {

                        //        }
                        //        else
                        //        {
                        //            break;
                        //        }
                        //    }
                        //    //Thread.Sleep(500);
                        //    //Check_DSAZ334A_state(visa_name.Text, ":PDER?");

                        //    DSAZ334A(visa_name.Text, ":LANE1:EQUalizer:DFE:TAP:AUTomatic");
                        //    Check_DSAZ334A_state(visa_name.Text, ":PDER?");
                        //    Check_DSAZ334A_state(visa_name.Text, ":PDER?");
                        //    DSAZ334A(visa_name.Text, ":LANE1:EQUalizer:DFE:TAP:DELay:AUTomatic");
                        //    Check_DSAZ334A_state(visa_name.Text, ":PDER?");
                        //    Check_DSAZ334A_state(visa_name.Text, ":PDER?");
                        //    //MessageBox.Show("11111");
                        //    for (int k = 0; k < 5; k++)
                        //    {
                        //        DSAZ334A(visa_name.Text, ":CDISplay");
                        //        //Thread.Sleep(1000);
                        //        DSAZ334A(visa_name.Text, ":SINGle");
                        //        Thread.Sleep(1000);
                        //        Check_DSAZ334AOLD_state(visa_name.Text, ":ADER?");
                        //        Thread.Sleep(1000);
                        //        while (true) //檢查run是否完成
                        //        {
                        //            Check_DSAZ334AOLD_state(visa_name.Text, ":ADER?");
                        //            Thread.Sleep(100);
                        //            if (PDER_value == "" || PDER_value == "0")
                        //            {

                        //            }
                        //            else
                        //            {
                        //                break;
                        //            }
                        //        }
                        //        Thread.Sleep(500);
                        //        //MessageBox.Show("2222");
                        //        //Check_DSAZ334A_state(visa_name.Text, ":ADER?");



                        //        DSAZ334A_query(visa_name.Text, ":MEASure:RESults?"); //,shawn,2020.11.11
                        //        //Check_DSAZ334A_state(visa_name.Text, ":PDER?");
                        //        DSAZ334A_query_value = DSAZ334A_query_value.Replace(",9.99999E+37", "");
                        //        pos1 = DSAZ334A_query_value.IndexOf("width");
                        //        pos2 = DSAZ334A_query_value.IndexOf(",", pos1);
                        //        pos3 = DSAZ334A_query_value.IndexOf("E", pos1);
                        //        pos4 = DSAZ334A_query_value.IndexOf(",", pos3);
                        //        g = Convert.ToDouble(DSAZ334A_query_value.Substring(pos3 + 1, pos4 - pos3));
                        //        g = Math.Pow(10, g) * Math.Pow(10, 11);
                        //        width[k] = Math.Round(Convert.ToDouble(DSAZ334A_query_value.Substring(pos2 + 1, pos3 - pos2 - 1)) * g, 3);

                        //        pos1 = DSAZ334A_query_value.IndexOf("height");
                        //        pos2 = DSAZ334A_query_value.IndexOf(",", pos1);
                        //        pos3 = DSAZ334A_query_value.IndexOf("E", pos1);
                        //        pos4 = DSAZ334A_query_value.IndexOf(",", pos3);
                        //        g = Convert.ToDouble(DSAZ334A_query_value.Substring(pos3 + 1, pos4 - pos3));
                        //        g = Math.Pow(10, g) * Math.Pow(10, 1);
                        //        height[k] = Math.Round(Convert.ToDouble(DSAZ334A_query_value.Substring(pos2 + 1, pos3 - pos2 - 1)) * g, 3);
                        //        MessageBox.Show("33333");
                        //        switch (gain[j]) //"0.891", "0.794", "0.708", "0.631", "0.562", "0.501", "0.447" 
                        //        {
                        //            case "0.891":
                        //                if (freq == "Freq20p6G" || freq == "Freq20G")
                        //                {
                        //                    ctle_excel_write(37, 5 + 2 * k, height[k], LAN);
                        //                    ctle_excel_write(37, 6 + 2 * k, width[k], LAN);
                        //                }
                        //                else
                        //                {
                        //                    ctle_excel_write(7, 5 + 2 * k, height[k], LAN);
                        //                    ctle_excel_write(7, 6 + 2 * k, width[k], LAN);
                        //                }

                        //                break;
                        //            case "0.794":
                        //                if (freq == "Freq20p6G" || freq == "Freq20G")
                        //                {
                        //                    ctle_excel_write(38, 5 + 2 * k, height[k], LAN);
                        //                    ctle_excel_write(38, 6 + 2 * k, width[k], LAN);
                        //                }
                        //                else
                        //                {
                        //                    ctle_excel_write(8, 5 + 2 * k, height[k], LAN);
                        //                    ctle_excel_write(8, 6 + 2 * k, width[k], LAN);
                        //                }
                        //                break;
                        //            case "0.708":
                        //                if (freq == "Freq20p6G" || freq == "Freq20G")
                        //                {
                        //                    ctle_excel_write(39, 5 + 2 * k, height[k], LAN);
                        //                    ctle_excel_write(39, 6 + 2 * k, width[k], LAN);
                        //                }
                        //                else
                        //                {
                        //                    ctle_excel_write(9, 5 + 2 * k, height[k], LAN);
                        //                    ctle_excel_write(9, 6 + 2 * k, width[k], LAN);
                        //                }
                        //                break;
                        //            case "0.631":
                        //                if (freq == "Freq20p6G" || freq == "Freq20G")
                        //                {
                        //                    ctle_excel_write(40, 5 + 2 * k, height[k], LAN);
                        //                    ctle_excel_write(40, 6 + 2 * k, width[k], LAN);
                        //                }
                        //                else
                        //                {
                        //                    ctle_excel_write(10, 5 + 2 * k, height[k], LAN);
                        //                    ctle_excel_write(10, 6 + 2 * k, width[k], LAN);
                        //                }
                        //                break;
                        //            case "0.562":
                        //                if (freq == "Freq20p6G" || freq == "Freq20G")
                        //                {
                        //                    ctle_excel_write(41, 5 + 2 * k, height[k], LAN);
                        //                    ctle_excel_write(41, 6 + 2 * k, width[k], LAN);
                        //                }
                        //                else
                        //                {
                        //                    ctle_excel_write(11, 5 + 2 * k, height[k], LAN);
                        //                    ctle_excel_write(11, 6 + 2 * k, width[k], LAN);
                        //                }
                        //                break;
                        //            case "0.501":
                        //                if (freq == "Freq20p6G" || freq == "Freq20G")
                        //                {
                        //                    ctle_excel_write(42, 5 + 2 * k, height[k], LAN);
                        //                    ctle_excel_write(42, 6 + 2 * k, width[k], LAN);
                        //                }
                        //                else
                        //                {
                        //                    ctle_excel_write(12, 5 + 2 * k, height[k], LAN);
                        //                    ctle_excel_write(12, 6 + 2 * k, width[k], LAN);
                        //                }
                        //                break;
                        //            case "0.447":
                        //                if (freq == "Freq20p6G" || freq == "Freq20G")
                        //                {
                        //                    ctle_excel_write(43, 5 + 2 * k, height[k], LAN);
                        //                    ctle_excel_write(43, 6 + 2 * k, width[k], LAN);
                        //                }
                        //                else
                        //                {
                        //                    ctle_excel_write(13, 5 + 2 * k, height[k], LAN);
                        //                    ctle_excel_write(13, 6 + 2 * k, width[k], LAN);
                        //                }
                        //                break;
                        //        }

                        //    }
                        //    array[j] = ((width[0] + width[1] + width[2] + width[3] + width[4]) / 5) * ((height[0] + height[1] + height[2] + height[3] + height[4]) / 5);

                        //    if (j == 1)
                        //    {
                        //        if (array[j - 1] > array[j])
                        //        {
                        //            if (freq == "Freq20p6G" || freq == "Freq20G")
                        //            {
                        //                DC_gain_20G = gain[j - 1];
                        //            }
                        //            else
                        //            {
                        //                DC_gain_10G = gain[j - 1];
                        //            }

                        //            break;
                        //        }
                        //    }
                        //    if (j >= 2)
                        //    {
                        //        if (array[j - 1] > array[j - 2] && array[j - 1] > array[j])
                        //        {
                        //            if (freq == "Freq20p6G" || freq == "Freq20G")
                        //            {
                        //                DC_gain_20G = gain[j - 1];
                        //            }
                        //            else
                        //            {
                        //                DC_gain_10G = gain[j - 1];
                        //            }
                        //            break;
                        //        }
                        //    }

                        //}
                        //DSAZ334A(visa_name.Text, ":CDISplay");
                        //MessageBox.Show("4444");
                        //if (freq == "Freq20p6G" || freq == "Freq20G")
                        //{
                        //    DSAZ334A(visa_name.Text, ":LANE1:EQUalizer:CTLE:DCGain " + DC_gain_20G);
                        //}
                        //else
                        //{
                        //    DSAZ334A(visa_name.Text, ":LANE1:EQUalizer:CTLE:DCGain " + DC_gain_10G);
                        //}
                        //MessageBox.Show("5555555");
                        //Check_DSAZ334A_state(visa_name.Text, ":PDER?");
                        //DSAZ334A(visa_name.Text, ":SINGle");
                        //Check_DSAZ334A_state(visa_name.Text, ":PDER?");

                        //DSAZ334A(visa_name.Text, ":LANE1:EQUalizer:DFE:TAP:AUTomatic");
                        //Check_DSAZ334A_state(visa_name.Text, ":PDER?");
                        //Check_DSAZ334A_state(visa_name.Text, ":PDER?");
                        //DSAZ334A(visa_name.Text, ":LANE1:EQUalizer:DFE:TAP:DELay:AUTomatic");
                        //Check_DSAZ334A_state(visa_name.Text, ":PDER?");
                        //Check_DSAZ334A_state(visa_name.Text, ":PDER?");

                        //DSAZ334A(visa_name.Text, ":CDISplay");
                        //DSAZ334A(visa_name.Text, ":MTESt:STARt");
                        //Check_DSAZ334A_state(visa_name.Text, ":PDER?");
                        //Check_DSAZ334A_state(visa_name.Text, ":PDER?");
                        //MessageBox.Show("already save");
                        
                        for (int k = 0; k < 5; k++)
                        {
                            DSAZ334A(visa_name.Text, ":CDISplay");
                            //Thread.Sleep(1000);
                            
                            DSAZ334A(visa_name.Text, ":SINGle");
                            Thread.Sleep(1000);
                            Check_DSAZ334AOLD_state(visa_name.Text, ":ADER?");
                            Thread.Sleep(1000);
                            
                            //while (true) //檢查run是否完成
                            //{
                            //    Check_DSAZ334AOLD_state(visa_name.Text, ":ADER?");
                            //    Thread.Sleep(100);
                            //    if (PDER_value == "" || PDER_value == "0")
                            //    {
                            //        MessageBox.Show("none");
                            //    }
                            //    else
                            //    {
                            //        MessageBox.Show("break");
                            //        break;
                            //    }
                            //}
                            Thread.Sleep(5000);

                            
                            Thread.Sleep(3000);

                            if (LAN == "lan0")
                            {
                                if (port == "port_a")
                                {
                                    //MessageBox.Show("portA");
                                    if (freq == "Freq20G")
                                    {
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 20G TP3 L0\",PNG,SCR,ON,NORMal");
                                        //Thread.Sleep(3000);
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs31_trial_1\",BIN,ON");
                                        //Thread.Sleep(3000);
                                        DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_20G_L0\\tp3_prbs31_trial_" + (k + 1).ToString() + "\"" + ",BIN,ON");
                                        Thread.Sleep(3000);
                                    }
                                    else if (freq == "Freq20p6G")
                                    {
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 20.6G TP3 L0\",PNG,SCR,ON,NORMal");
                                        //Thread.Sleep(3000);
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs31_trial_1\",BIN,ON");
                                        //Thread.Sleep(3000);
                                        DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_20p6G_L0\\tp3_prbs31_trial_" + (k + 1).ToString() + "\"" + ",BIN,ON");
                                        Thread.Sleep(3000);
                                    }
                                    else if (freq == "Freq10p3G")
                                    {
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 10.3G TP3 L0\",PNG,SCR,ON,NORMal");
                                        //Thread.Sleep(3000);
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs31_trial_1\",BIN,ON");
                                        //Thread.Sleep(3000);
                                        DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_10p3G_L0\\tp3_prbs31_trial_" + (k + 1).ToString() + "\"" + ",BIN,ON");
                                        Thread.Sleep(3000);
                                    }
                                    else if (freq == "Freq10G")
                                    {
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 10G TP3 L0\",PNG,SCR,ON,NORMal");
                                        //Thread.Sleep(3000);
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs31_trial_1\",BIN,ON");
                                        //Thread.Sleep(3000);
                                        DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_10G_L0\\tp3_prbs31_trial_"+ (k+1) .ToString() +"\"" + ",BIN,ON");
                                        Thread.Sleep(3000);
                                    }
                                }
                                else if (port == "port_b")
                                {
                                    if (freq == "Freq20G")
                                    {
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 20G TP3 L0\",PNG,SCR,ON,NORMal");
                                        //Thread.Sleep(3000);
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs31_trial_1\",BIN,ON");
                                        //Thread.Sleep(3000);
                                        DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_20G_L0\\tp3_prbs31_trial_" + (k + 1).ToString() + "\"" + ",BIN,ON");
                                        Thread.Sleep(3000);
                                    }
                                    else if (freq == "Freq20p6G")
                                    {
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 20.6G TP3 L0\",PNG,SCR,ON,NORMal");
                                        //Thread.Sleep(3000);
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs31_trial_1\",BIN,ON");
                                        //Thread.Sleep(3000);
                                        DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_20p6G_L0\\tp3_prbs31_trial_" + (k + 1).ToString() + "\"" + ",BIN,ON");
                                        Thread.Sleep(3000);
                                    }
                                    else if (freq == "Freq10p3G")
                                    {
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 10.3G TP3 L0\",PNG,SCR,ON,NORMal");
                                        //Thread.Sleep(3000);
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs31_trial_1\",BIN,ON");
                                        //Thread.Sleep(3000);
                                        DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_10p3G_L0\\tp3_prbs31_trial_" + (k + 1).ToString() + "\"" + ",BIN,ON");
                                        Thread.Sleep(3000);
                                    }
                                    else if (freq == "Freq10G")
                                    {
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 10G TP3 L0\",PNG,SCR,ON,NORMal");
                                        //Thread.Sleep(3000);
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs31_trial_1\",BIN,ON");
                                        //Thread.Sleep(3000);
                                        DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_10G_L0\\tp3_prbs31_trial_" + (k + 1).ToString() + "\"" + ",BIN,ON");
                                        Thread.Sleep(3000);
                                    }
                                }
                                else if (port == "port_c")
                                {
                                    if (freq == "Freq20G")
                                    {
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 20G TP3 L0\",PNG,SCR,ON,NORMal");
                                        //Thread.Sleep(3000);
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs31_trial_1\",BIN,ON");
                                        //Thread.Sleep(3000);
                                        DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_20G_L0\\tp3_prbs31_trial_" + (k + 1).ToString() + "\"" + ",BIN,ON");
                                        Thread.Sleep(3000);
                                    }
                                    else if (freq == "Freq20p6G")
                                    {
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 20.6G TP3 L0\",PNG,SCR,ON,NORMal");
                                        //Thread.Sleep(3000);
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs31_trial_1\",BIN,ON");
                                        //Thread.Sleep(3000);
                                        DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_20p6G_L0\\tp3_prbs31_trial_" + (k + 1).ToString() + "\"" + ",BIN,ON");
                                        Thread.Sleep(3000);
                                    }
                                    else if (freq == "Freq10p3G")
                                    {
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 10.3G TP3 L0\",PNG,SCR,ON,NORMal");
                                        //Thread.Sleep(3000);
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs31_trial_1\",BIN,ON");
                                        //Thread.Sleep(3000);
                                        DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANne3l1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_10p3G_L0\\tp3_prbs31_trial_" + (k + 1).ToString() + "\"" + ",BIN,ON");
                                        Thread.Sleep(3000);
                                    }
                                    else if (freq == "Freq10G")
                                    {
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 10G TP3 L0\",PNG,SCR,ON,NORMal");
                                        //Thread.Sleep(3000);
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs31_trial_1\",BIN,ON");
                                        //Thread.Sleep(3000);
                                        DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_10G_L0\\tp3_prbs31_trial_" + (k + 1).ToString() + "\"" + ",BIN,ON");
                                        Thread.Sleep(3000);
                                    }
                                }
                                else if (port == "port_d")
                                {
                                    if (freq == "Freq20G")
                                    {
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 20G TP3 L0\",PNG,SCR,ON,NORMal");
                                        //Thread.Sleep(3000);
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs31_trial_1\",BIN,ON");
                                        //Thread.Sleep(3000);
                                        DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_20G_L0\\tp3_prbs31_trial_" + (k + 1).ToString() + "\"" + ",BIN,ON");
                                        Thread.Sleep(3000);
                                    }
                                    else if (freq == "Freq20p6G")
                                    {
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 20.6G TP3 L0\",PNG,SCR,ON,NORMal");
                                        //Thread.Sleep(3000);
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs31_trial_1\",BIN,ON");
                                        //Thread.Sleep(3000);
                                        DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_20p6G_L0\\tp3_prbs31_trial_" + (k + 1).ToString() + "\"" + ",BIN,ON");
                                        Thread.Sleep(3000);
                                    }
                                    else if (freq == "Freq10p3G")
                                    {
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 10.3G TP3 L0\",PNG,SCR,ON,NORMal");
                                        //Thread.Sleep(3000);
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs31_trial_1\",BIN,ON");
                                        //Thread.Sleep(3000);
                                        DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_10p3G_L0\\tp3_prbs31_trial_" + (k + 1).ToString() + "\"" + ",BIN,ON");
                                        Thread.Sleep(3000);
                                    }
                                    else if (freq == "Freq10G")
                                    {
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 10G TP3 L0\",PNG,SCR,ON,NORMal");
                                        //Thread.Sleep(3000);
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs31_trial_1\",BIN,ON");
                                        //Thread.Sleep(3000);
                                        DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_10G_L0\\tp3_prbs31_trial_" + (k + 1).ToString() + "\"" + ",BIN,ON");
                                        Thread.Sleep(3000);
                                    }
                                }
                            }
                            else
                            {
                                if (port == "port_a")
                                {
                                    if (freq == "Freq20G")
                                    {
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 20G TP3 L0\",PNG,SCR,ON,NORMal");
                                        //Thread.Sleep(3000);
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs31_trial_1\",BIN,ON");
                                        //Thread.Sleep(3000);
                                        DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_20G_L1\\tp3_prbs31_trial_" + (k + 1).ToString() + "\"" + ",BIN,ON");
                                        Thread.Sleep(3000);
                                    }
                                    else if (freq == "Freq20p6G")
                                    {
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 20.6G TP3 L0\",PNG,SCR,ON,NORMal");
                                        //Thread.Sleep(3000);
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs31_trial_1\",BIN,ON");
                                        //Thread.Sleep(3000);
                                        DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_20p6G_L1\\tp3_prbs31_trial_" + (k + 1).ToString() + "\"" + ",BIN,ON");
                                        Thread.Sleep(3000);
                                    }
                                    else if (freq == "Freq10p3G")
                                    {
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 10.3G TP3 L0\",PNG,SCR,ON,NORMal");
                                        //Thread.Sleep(3000);
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs31_trial_1\",BIN,ON");
                                        //Thread.Sleep(3000);
                                        DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_10p3G_L1\\tp3_prbs31_trial_" + (k + 1).ToString() + "\"" + ",BIN,ON");
                                        Thread.Sleep(3000);
                                    }
                                    else if (freq == "Freq10G")
                                    {
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 10G TP3 L0\",PNG,SCR,ON,NORMal");
                                        //Thread.Sleep(3000);
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs31_trial_1\",BIN,ON");
                                        //Thread.Sleep(3000);
                                        DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_10G_L1\\tp3_prbs31_trial_" + (k + 1).ToString() + "\"" + ",BIN,ON");
                                        Thread.Sleep(3000);
                                    }
                                }
                                else if (port == "port_b")
                                {
                                    if (freq == "Freq20G")
                                    {
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 20G TP3 L0\",PNG,SCR,ON,NORMal");
                                        //Thread.Sleep(3000);
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs31_trial_1\",BIN,ON");
                                        //Thread.Sleep(3000);
                                        DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_20G_L1\\tp3_prbs31_trial_" + (k + 1).ToString() + "\"" + ",BIN,ON");
                                        Thread.Sleep(3000);
                                    }
                                    else if (freq == "Freq20p6G")
                                    {
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 20.6G TP3 L0\",PNG,SCR,ON,NORMal");
                                        //Thread.Sleep(3000);
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs31_trial_1\",BIN,ON");
                                        //Thread.Sleep(3000);
                                        DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_20p6G_L1\\tp3_prbs31_trial_" + (k + 1).ToString() + "\"" + ",BIN,ON");
                                        Thread.Sleep(3000);
                                    }
                                    else if (freq == "Freq10p3G")
                                    {
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 10.3G TP3 L0\",PNG,SCR,ON,NORMal");
                                        //Thread.Sleep(3000);
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs31_trial_1\",BIN,ON");
                                        //Thread.Sleep(3000);
                                        DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_10p3G_L1\\tp3_prbs31_trial_" + (k + 1).ToString() + "\"" + ",BIN,ON");
                                        Thread.Sleep(3000);
                                    }
                                    else if (freq == "Freq10G")
                                    {
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 10G TP3 L0\",PNG,SCR,ON,NORMal");
                                        //Thread.Sleep(3000);
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs31_trial_1\",BIN,ON");
                                        //Thread.Sleep(3000);
                                        DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_10G_L1\\tp3_prbs31_trial_" + (k + 1).ToString() + "\"" + ",BIN,ON");
                                        Thread.Sleep(3000);
                                    }
                                }
                                else if (port == "port_c")
                                {
                                    if (freq == "Freq20G")
                                    {
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 20G TP3 L0\",PNG,SCR,ON,NORMal");
                                        //Thread.Sleep(3000);
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs31_trial_1\",BIN,ON");
                                        //Thread.Sleep(3000);
                                        DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_20G_L1\\tp3_prbs31_trial_" + (k + 1).ToString() + "\"" + ",BIN,ON");
                                        Thread.Sleep(3000);
                                    }
                                    else if (freq == "Freq20p6G")
                                    {
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 20.6G TP3 L0\",PNG,SCR,ON,NORMal");
                                        //Thread.Sleep(3000);
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs31_trial_1\",BIN,ON");
                                        //Thread.Sleep(3000);
                                        DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_20p6G_L1\\tp3_prbs31_trial_" + (k + 1).ToString() + "\"" + ",BIN,ON");
                                        Thread.Sleep(3000);
                                    }
                                    else if (freq == "Freq10p3G")
                                    {
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 10.3G TP3 L0\",PNG,SCR,ON,NORMal");
                                        //Thread.Sleep(3000);
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs31_trial_1\",BIN,ON");
                                        //Thread.Sleep(3000);
                                        DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_10p3G_L1\\tp3_prbs31_trial_" + (k + 1).ToString() + "\"" + ",BIN,ON");
                                        Thread.Sleep(3000);
                                    }
                                    else if (freq == "Freq10G")
                                    {
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 10G TP3 L0\",PNG,SCR,ON,NORMal");
                                        //Thread.Sleep(3000);
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs31_trial_1\",BIN,ON");
                                        //Thread.Sleep(3000);
                                        DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_10G_L1\\tp3_prbs31_trial_" + (k + 1).ToString() + "\"" + ",BIN,ON");
                                        Thread.Sleep(3000);
                                    }
                                }
                                else if (port == "port_d")
                                {
                                    if (freq == "Freq20G")
                                    {
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 20G TP3 L0\",PNG,SCR,ON,NORMal");
                                        //Thread.Sleep(3000);
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs31_trial_1\",BIN,ON");
                                        //Thread.Sleep(3000);
                                        DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_20G_L1\\tp3_prbs31_trial_" + (k + 1).ToString() + "\"" + ",BIN,ON");
                                        Thread.Sleep(3000);
                                    }
                                    else if (freq == "Freq20p6G")
                                    {
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 20.6G TP3 L0\",PNG,SCR,ON,NORMal");
                                        //Thread.Sleep(3000);
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs31_trial_1\",BIN,ON");
                                        //Thread.Sleep(3000);
                                        DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_20p6G_L1\\tp3_prbs31_trial_" + (k + 1).ToString() + "\"" + ",BIN,ON");
                                        Thread.Sleep(3000);
                                    }
                                    else if (freq == "Freq10p3G")
                                    {
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 10.3G TP3 L0\",PNG,SCR,ON,NORMal");
                                        //Thread.Sleep(3000);
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs31_trial_1\",BIN,ON");
                                        //Thread.Sleep(3000);
                                        DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_10p3G_L1\\tp3_prbs31_trial_" + (k + 1).ToString() + "\"" + ",BIN,ON");
                                        Thread.Sleep(3000);
                                    }
                                    else if (freq == "Freq10G")
                                    {
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"EYE 10G TP3 L0\",PNG,SCR,ON,NORMal");
                                        //Thread.Sleep(3000);
                                        //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs31_trial_1\",BIN,ON");
                                        //Thread.Sleep(3000);
                                        DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_10G_L1\\tp3_prbs31_trial_" + (k + 1).ToString() + "\"" + ",BIN,ON");
                                        Thread.Sleep(3000);
                                    }
                                }
                            }
                            //MessageBox.Show("EYE_Passive");
                        }
                        break;

                    case "Jitter_Passive":
                        DSAZ334A(visa_name.Text, "*RST");
                        Thread.Sleep(3000);
                        DSAZ334A(visa_name.Text, ":DISK:LOAD \"C:\\USB4_SIGTEST\\" + cio + "\\" + "TP3_PRBS15_5times_PRBS31.set\"");
                        if (freq == "Freq10p3G")
                        {
                            command = "-Dp " + Dp_number + " -L0 -L1 -L All -U Disabled -Tt TX -D Router -Pa PRBS15 -T";
                        }
                        else if (freq == "Freq10G")
                        {
                            command = "-Dp " + Dp_number + " -L0 -L1 -L All -U Disabled -Tt TX -D Router -Pa PRBS15";
                        }
                        else if (freq == "Freq20p6G")
                        {
                            command = "-Dp " + Dp_number + " -L0 -L1 -L All -U Disabled -Tt TX -D Router -Pa PRBS15 -G3 -T";
                        }
                        else
                        {
                            command = "-Dp " + Dp_number + " -L0 -L1 -L All -U Disabled -Tt TX -D Router -Pa PRBS15 -G3";
                        }
                        USB4ElectricalTestToolCLI(command);
                        Thread.Sleep(5000);
                        //if (freq == "Freq20G" || freq == "Freq20p6G")
                        //{
                        //    DSAZ334A(visa_name.Text, ":LANE1:EQUalizer:CTLE:DCGain " + DC_gain_20G);
                        //}
                        //else
                        //{
                        //    DSAZ334A(visa_name.Text, ":LANE1:EQUalizer:CTLE:DCGain " + DC_gain_10G);
                        //}
                        Check_DSAZ334A_state(visa_name.Text, ":PDER?");
                        DSAZ334A(visa_name.Text, ":SINGle");
                        Check_DSAZ334A_state(visa_name.Text, ":PDER?");
                        if (LAN == "lan0")
                        {
                            if (port == "port_a")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 20G TP3 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 20G TP3 L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_10G_L1\\tp3_prbs31_trial_" + (k + 1).ToString() + "\"" + ",BIN,ON");
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_20G_L0\\tp3_prbs15\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 20.6G TP3 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 20.6G TP3 L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs15\",BIN,ON");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_20p6G_L0\\tp3_prbs15\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 10.3G TP3 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 10.3G TP3 L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs15\",BIN,ON");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_10p3G_L0\\tp3_prbs15\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 10G TP3 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 10G TP3 L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs15\",BIN,ON");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_10G_L0\\tp3_prbs15\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                            }
                            else if (port == "port_b")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 20G TP3 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 20G TP3 L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_10G_L1\\tp3_prbs31_trial_" + (k + 1).ToString() + "\"" + ",BIN,ON");
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_20G_L0\\tp3_prbs15\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 20.6G TP3 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 20.6G TP3 L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs15\",BIN,ON");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_20p6G_L0\\tp3_prbs15\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 10.3G TP3 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 10.3G TP3 L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs15\",BIN,ON");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_10p3G_L0\\tp3_prbs15\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 10G TP3 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 10G TP3 L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs15\",BIN,ON");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_10G_L0\\tp3_prbs15\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                            }
                            else if (port == "port_c")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 20G TP3 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 20G TP3 L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_10G_L1\\tp3_prbs31_trial_" + (k + 1).ToString() + "\"" + ",BIN,ON");
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_20G_L0\\tp3_prbs15\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 20.6G TP3 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 20.6G TP3 L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs15\",BIN,ON");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_20p6G_L0\\tp3_prbs15\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 10.3G TP3 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 10.3G TP3 L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs15\",BIN,ON");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_10p3G_L0\\tp3_prbs15\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 10G TP3 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 10G TP3 L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs15\",BIN,ON");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_10G_L0\\tp3_prbs15\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                            }
                            else if (port == "port_d")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 20G TP3 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 20G TP3 L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_10G_L1\\tp3_prbs31_trial_" + (k + 1).ToString() + "\"" + ",BIN,ON");
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_20G_L0\\tp3_prbs15\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 20.6G TP3 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 20.6G TP3 L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs15\",BIN,ON");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_20p6G_L0\\tp3_prbs15\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 10.3G TP3 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 10.3G TP3 L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs15\",BIN,ON");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_10p3G_L0\\tp3_prbs15\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 10G TP3 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 10G TP3 L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs15\",BIN,ON");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_10G_L0\\tp3_prbs15\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                            }
                        }
                        else
                        {
                            if (port == "port_a")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 20G TP3 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 20G TP3 L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_10G_L1\\tp3_prbs31_trial_" + (k + 1).ToString() + "\"" + ",BIN,ON");
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_20G_L1\\tp3_prbs15\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 20.6G TP3 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 20.6G TP3 L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs15\",BIN,ON");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_20p6G_L1\\tp3_prbs15\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 10.3G TP3 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 10.3G TP3 L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs15\",BIN,ON");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_10p3G_L1\\tp3_prbs15\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 10G TP3 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 10G TP3 L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs15\",BIN,ON");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_10G_L1\\tp3_prbs15\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                            }
                            else if (port == "port_b")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 20G TP3 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 20G TP3 L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_10G_L1\\tp3_prbs31_trial_" + (k + 1).ToString() + "\"" + ",BIN,ON");
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_20G_L1\\tp3_prbs15\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 20.6G TP3 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 20.6G TP3 L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs15\",BIN,ON");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_20p6G_L1\\tp3_prbs15\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 10.3G TP3 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 10.3G TP3 L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs15\",BIN,ON");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_10p3G_L1\\tp3_prbs15\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 10G TP3 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 10G TP3 L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs15\",BIN,ON");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_10G_L1\\tp3_prbs15\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                            }
                            else if (port == "port_c")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 20G TP3 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 20G TP3 L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_10G_L1\\tp3_prbs31_trial_" + (k + 1).ToString() + "\"" + ",BIN,ON");
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_20G_L1\\tp3_prbs15\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 20.6G TP3 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 20.6G TP3 L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs15\",BIN,ON");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_20p6G_L1\\tp3_prbs15\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 10.3G TP3 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 10.3G TP3 L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs15\",BIN,ON");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_10p3G_L1\\tp3_prbs15\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 10G TP3 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 10G TP3 L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs15\",BIN,ON");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_10G_L1\\tp3_prbs15\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                            }
                            else if (port == "port_d")
                            {
                                if (freq == "Freq20G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 20G TP3 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 20G TP3 L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_10G_L1\\tp3_prbs31_trial_" + (k + 1).ToString() + "\"" + ",BIN,ON");
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_20G_L1\\tp3_prbs15\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq20p6G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 20.6G TP3 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 20.6G TP3 L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs15\",BIN,ON");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_20p6G_L1\\tp3_prbs15\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10p3G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 10.3G TP3 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 10.3G TP3 L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs15\",BIN,ON");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_10p3G_L1\\tp3_prbs15\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                                else if (freq == "Freq10G")
                                {
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"Jitter 10G TP3 L0\",PNG,SCR,ON,NORMal");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:JITTer \"Jitter 10G TP3 L0\"");
                                    //Thread.Sleep(3000);
                                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"tp3_prbs15\",BIN,ON");
                                    //Thread.Sleep(3000);
                                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\TP3_10G_L1\\tp3_prbs15\",BIN,ON");
                                    Thread.Sleep(3000);
                                }
                            }
                        }
                        //MessageBox.Show("Jitter_Passive");
                        break;
                    //case "EQ_Test":
                    //    DSAZ334A(visa_name.Text, "*RST");
                    //    Thread.Sleep(3000);
                    //    DSAZ334A(visa_name.Text, ":DISK:LOAD \"C:\\USB4_SIGTEST\\" + cio + "\\" + "TP2_PRBS15_PRBS31_SQ128.set\"");
                    //    Check_DSAZ334A_state(visa_name.Text, ":PDER?");
                    //    if (freq == "Freq20p6G")
                    //    {
                    //        command = "USB4ElectricalTestToolCLI.exe -Dp " + Dp_number + " -L0 -L1 -L All -G3 -Pa PRBS15 -Tt TX -M -Pr 0 -De -Ps -D Router -T";
                    //    }
                    //    else if (freq == "Freq20G")
                    //    {
                    //        command = "USB4ElectricalTestToolCLI.exe -Dp " + Dp_number + " -L0 -L1 -L All -G3 -Pa PRBS15 -Tt TX -M -Pr 0 -De -Ps -D Router";
                    //    }
                    //    else if (freq == "Freq10p3G")
                    //    {
                    //        command = "USB4ElectricalTestToolCLI.exe -Dp " + Dp_number + " -L0 -L1 -L All -Pa PRBS15 -Tt TX -M -Pr 0 -De -Ps -D Router -T";
                    //    }
                    //    else
                    //    {
                    //        command = "USB4ElectricalTestToolCLI.exe -Dp " + Dp_number + " -L0 -L1 -L All -Pa PRBS15 -Tt TX -M -Pr 0 -De -Ps -D Router";
                    //    }
                    //    excute_cmd_tx_preset(command, LAN, freq, port);
                    //    //MessageBox.Show("Preset");
                    //    break;
                }
            }

        }
        void USB4ElectricalTestToolCLI(string parameter)
        {
            
            String log = null;
            Process p = new Process();
            int show_SBU = 0;
            p.StartInfo.FileName = "USB4ElectricalTestToolCLI.exe";

            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardInput = true;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.RedirectStandardError = true;
            p.StartInfo.CreateNoWindow = true; //不跳出cmd視窗
            if(tgl_mode.Checked == true)
            {
                p.StartInfo.Arguments = parameter+" -tgl";
            }
            else
            {
                p.StartInfo.Arguments = parameter;
            }

            p.Start();
            while(true)
            {
                try
                {
                    log = p.StandardOutput.ReadLine();
                }
                catch
                {
                    log = null;
                }
                if (log != null)
                {
                    if (log.Contains("RUNNING") && show_SBU == 1)
                    {
                        if (MessageBox.Show("Please check the fixture board.\nIf you want to contiue testing,\nplease push \"Yes\" button, else push \"No\" button. ", "Error message", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            p.Close();
                            Thread.Sleep(1000);
                            p.Start();
                        }
                        else
                        {
                            thread_for_testing.Abort();
                            break;
                        }
                    }
                    if (log.Contains("SBU MUX Enabled"))
                    {
                        show_SBU = 1;
                    }
                    else
                    {
                        show_SBU = 0;
                    }
                }

                if (log == null)
                {
                    break;
                }
                this.Invoke((MethodInvoker)delegate ()
                {
                    Log.Items.Add(log);
                    Log.TopIndex = Log.Items.Count - 1;
                });
            }
            p.WaitForExit();
            p.Close();
            
        }

        void focus_app()
        {
            Process p = new Process();
            p.StartInfo.FileName = "focus windows app.exe";
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardInput = true;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.RedirectStandardError = true;
            p.StartInfo.CreateNoWindow = true; //不跳出視窗
            p.Start();
            p.WaitForExit();
            p.Close();
        }

        void excute_cmd_tx_EQ_Psoff_Deoff(string cmd_command, string freq)
        {
            //string new_cmd_command;

            Process p = new Process();
            String log = null;
            bool string_exist;

            if (freq == "Freq10G")
            {
                freq = "10G";
            }
            else if (freq == "Freq10p3G")
            {
                freq = "10p3G";
            }
            else if (freq == "Freq20G")
            {
                freq = "20G";
            }
            else if (freq == "Freq20p6G")
            {
                freq = "20p6G";
            }

            p.StartInfo.FileName = "cmd.exe";
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardInput = true;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.RedirectStandardError = true;
            p.StartInfo.CreateNoWindow = true; //不跳出cmd視窗
            //p.StartInfo.Arguments = parameter;
            p.Start();

            if (tgl_mode.Checked == true)
            {
                cmd_command = cmd_command + " -tgl";  
            }


            p.StandardInput.WriteLine(cmd_command);
            while (true)
            {
                log = p.StandardOutput.ReadLine();
                this.Invoke((MethodInvoker)delegate ()
                {
                    Log.Items.Add(log);
                    Log.TopIndex = Log.Items.Count - 1;
                });
                if ((string_exist = log.Contains("Set new TxFFE"))) //等待執行檔回傳"Set new TxFFE"後才可以繼續往下做
                {
                    break;
                }
                Thread.Sleep(100);
            }
            Thread.Sleep(1000);
            p.StandardInput.WriteLine("0");
            Thread.Sleep(1000);
            Check_DSAZ334A_state(visa_name.Text, ":PDER?");
            DSAZ334A(visa_name.Text, ":SINGle");
            Check_DSAZ334A_state(visa_name.Text, ":PDER?");
            DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\EQ_" + freq + "_L0\\preset_0_preshoot_off_deemphasis_off" + "\",BIN,ON");
            Thread.Sleep(3000);
            DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"C:\\Users\\Public\\Documents\\Infiniium\\EQ_" + freq + "_L1\\preset_0_preshoot_off_deemphasis_off" + "\",BIN,ON");
            Thread.Sleep(3000);
            DSAZ334A(visa_name.Text, ":CDISplay");

            Thread.Sleep(5000);
            p.StandardInput.WriteLine("15");
            Thread.Sleep(1000);
            Check_DSAZ334A_state(visa_name.Text, ":PDER?");
            DSAZ334A(visa_name.Text, ":SINGle");
            Check_DSAZ334A_state(visa_name.Text, ":PDER?");
            DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\EQ_" + freq + "_L0\\preset_15_preshoot_off_deemphasis_off" + "\",BIN,ON");
            Thread.Sleep(3000);
            DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"C:\\Users\\Public\\Documents\\Infiniium\\EQ_" + freq + "_L1\\preset_15_preshoot_off_deemphasis_off" + "\",BIN,ON");
            Thread.Sleep(3000);
            DSAZ334A(visa_name.Text, ":CDISplay");
            Thread.Sleep(3000);
            

            p.StandardInput.WriteLine("q");
            p.StandardInput.WriteLine("exit");
            //log = p.StandardOutput.ReadToEnd();
            p.WaitForExit();
            p.Close();
        }

        void excute_cmd_tx_EQ_Pson_Deoff(string cmd_command, string freq)
        {
            //string new_cmd_command;

            Process p = new Process();
            String log = null;
            bool string_exist;

            if (freq == "Freq10G")
            {
                freq = "10G";
            }
            else if (freq == "Freq10p3G")
            {
                freq = "10p3G";
            }
            else if (freq == "Freq20G")
            {
                freq = "20G";
            }
            else if (freq == "Freq20p6G")
            {
                freq = "20p6G";
            }

            p.StartInfo.FileName = "cmd.exe";
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardInput = true;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.RedirectStandardError = true;
            p.StartInfo.CreateNoWindow = true; //不跳出cmd視窗
            //p.StartInfo.Arguments = parameter;
            p.Start();

            if (tgl_mode.Checked == true)
            {
                cmd_command = cmd_command + " -tgl";
            }


            p.StandardInput.WriteLine(cmd_command + " -Ps");
            while (true)
            {
                log = p.StandardOutput.ReadLine();
                this.Invoke((MethodInvoker)delegate ()
                {
                    Log.Items.Add(log);
                    Log.TopIndex = Log.Items.Count - 1;
                });
                if ((string_exist = log.Contains("Set new TxFFE"))) //等待執行檔回傳"Set new TxFFE"後才可以繼續往下做
                {
                    break;
                }
                Thread.Sleep(100);
            }
            for (int run = 0; run < 16; run++)
            {
                Thread.Sleep(1000);
                p.StandardInput.WriteLine(run.ToString());
                Thread.Sleep(1000);
                Check_DSAZ334A_state(visa_name.Text, ":PDER?");
                DSAZ334A(visa_name.Text, ":SINGle");
                Check_DSAZ334A_state(visa_name.Text, ":PDER?");
                DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\EQ_" + freq + "_L0\\preset_" + run.ToString() + "_preshoot_on_deemphasis_off" + "\",BIN,ON");
                Thread.Sleep(3000);
                DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"C:\\Users\\Public\\Documents\\Infiniium\\EQ_" + freq + "_L1\\preset_" + run.ToString() + "_preshoot_on_deemphasis_off" + "\",BIN,ON");
                Thread.Sleep(3000);
                DSAZ334A(visa_name.Text, ":CDISplay");
                Thread.Sleep(3000);
            }


            p.StandardInput.WriteLine("q");
            p.StandardInput.WriteLine("exit");
            //log = p.StandardOutput.ReadToEnd();
            p.WaitForExit();
            p.Close();
        }


        void excute_cmd_tx_EQ_Pson_Deon(string cmd_command, string freq)
        {
            //string new_cmd_command;

            Process p = new Process();
            String log = null;
            bool string_exist;

            if (freq == "Freq10G")
            {
                freq = "10G";
            }
            else if (freq == "Freq10p3G")
            {
                freq = "10p3G";
            }
            else if (freq == "Freq20G")
            {
                freq = "20G";
            }
            else if (freq == "Freq20p6G")
            {
                freq = "20p6G";
            }

            p.StartInfo.FileName = "cmd.exe";
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardInput = true;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.RedirectStandardError = true;
            p.StartInfo.CreateNoWindow = true; //不跳出cmd視窗
            //p.StartInfo.Arguments = parameter;
            p.Start();

            if (tgl_mode.Checked == true)
            {
                cmd_command = cmd_command + " -tgl";
            }


            p.StandardInput.WriteLine(cmd_command + " -De" + " -Ps");
            while (true)
            {
                log = p.StandardOutput.ReadLine();
                this.Invoke((MethodInvoker)delegate ()
                {
                    Log.Items.Add(log);
                    Log.TopIndex = Log.Items.Count - 1;
                });
                if ((string_exist = log.Contains("Set new TxFFE"))) //等待執行檔回傳"Set new TxFFE"後才可以繼續往下做
                {
                    break;
                }
                Thread.Sleep(100);
            }

            for (int run = 0; run < 16; run++)
            {
                Thread.Sleep(1000);
                p.StandardInput.WriteLine(run.ToString());
                Thread.Sleep(1000);
                Check_DSAZ334A_state(visa_name.Text, ":PDER?");
                DSAZ334A(visa_name.Text, ":SINGle");
                Check_DSAZ334A_state(visa_name.Text, ":PDER?");
                DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\EQ_" + freq + "_L0\\preset_" + run.ToString() + "_preshoot_on_deemphasis_on" + "\",BIN,ON");
                Thread.Sleep(3000);
                DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"C:\\Users\\Public\\Documents\\Infiniium\\EQ_" + freq + "_L1\\preset_" + run.ToString() + "_preshoot_on_deemphasis_on" + "\",BIN,ON");
                Thread.Sleep(3000);
                DSAZ334A(visa_name.Text, ":CDISplay");
                Thread.Sleep(3000);
            }


            p.StandardInput.WriteLine("q");
            p.StandardInput.WriteLine("exit");
            //log = p.StandardOutput.ReadToEnd();
            p.WaitForExit();
            p.Close();
        }


        void excute_cmd_tx_EQ_Psoff_Deon(string cmd_command, string freq)
        {
            //string new_cmd_command;

            Process p = new Process();
            String log = null;
            bool string_exist;

            if (freq == "Freq10G")
            {
                freq = "10G";
            }
            else if (freq == "Freq10p3G")
            {
                freq = "10p3G";
            }
            else if (freq == "Freq20G")
            {
                freq = "20G";
            }
            else if (freq == "Freq20p6G")
            {
                freq = "20p6G";
            }

            p.StartInfo.FileName = "cmd.exe";
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardInput = true;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.RedirectStandardError = true;
            p.StartInfo.CreateNoWindow = true; //不跳出cmd視窗
            //p.StartInfo.Arguments = parameter;
            p.Start();

            if (tgl_mode.Checked == true)
            {
                cmd_command = cmd_command + " -tgl";
            }

            p.StandardInput.WriteLine(cmd_command + " -De");
            while (true)
            {
                log = p.StandardOutput.ReadLine();
                this.Invoke((MethodInvoker)delegate ()
                {
                    Log.Items.Add(log);
                    Log.TopIndex = Log.Items.Count - 1;
                });
                if ((string_exist = log.Contains("Set new TxFFE"))) //等待執行檔回傳"Set new TxFFE"後才可以繼續往下做
                {
                    break;
                }
                Thread.Sleep(100);
            }

            for (int run = 0; run < 16; run++)
            {
                Thread.Sleep(1000);
                p.StandardInput.WriteLine(run.ToString());
                Thread.Sleep(3000);
                Check_DSAZ334A_state(visa_name.Text, ":PDER?");
                DSAZ334A(visa_name.Text, ":SINGle");
                Check_DSAZ334A_state(visa_name.Text, ":PDER?");
                DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"C:\\Users\\Public\\Documents\\Infiniium\\EQ_" + freq + "_L0\\preset_" + run.ToString() + "_preshoot_off_deemphasis_on" + "\",BIN,ON");
                Thread.Sleep(3000);
                DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"C:\\Users\\Public\\Documents\\Infiniium\\EQ_" + freq + "_L1\\preset_" + run.ToString() + "_preshoot_off_deemphasis_on" + "\",BIN,ON");
                Thread.Sleep(3000);
                DSAZ334A(visa_name.Text, ":CDISplay");
                Thread.Sleep(3000);

            }
            p.StandardInput.WriteLine("q");







            p.StandardInput.WriteLine("q");
            p.StandardInput.WriteLine("exit");
            //log = p.StandardOutput.ReadToEnd();
            p.WaitForExit();
            p.Close();
        }

        void excute_cmd_tx_preset(string cmd_command, string LAN, string freq, string port)
        {

            Process p = new Process();
            String log = null;
            bool string_exist;


            if (LAN == "lan0")
            {
                LAN = "L0";
            }
            else if (LAN == "lan1")
            {
                LAN = "L1";
            }

            if (freq == "Freq10G")
            {
                freq = "10G";
            }
            else if (freq == "Freq10p3G")
            {
                freq = "10p3G";
            }
            else if (freq == "Freq20G")
            {
                freq = "20G";
            }
            else if (freq == "Freq20p6G")
            {
                freq = "20p6G";
            }

            if (port == "port_a")
            {
                port = "PA";
            }
            else if (port == "port_b")
            {
                port = "PB";
            }
            else if (port == "port_c")
            {
                port = "PC";
            }
            else if (port == "port_d")
            {
                port = "PD";
            }




            p.StartInfo.FileName = "cmd.exe";
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardInput = true;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.RedirectStandardError = true;
            p.StartInfo.CreateNoWindow = true; //不跳出cmd視窗
            //p.StartInfo.Arguments = parameter;
            p.Start();
            if (tgl_mode.Checked == true)
            {
                p.StandardInput.WriteLine(cmd_command+ " -tgl");
            }
            else
            {
                p.StandardInput.WriteLine(cmd_command);
            }

            while (true)
            {
                log = p.StandardOutput.ReadLine();
                this.Invoke((MethodInvoker)delegate ()
                {
                    Log.Items.Add(log);
                    Log.TopIndex = Log.Items.Count - 1;
                });
                if ((string_exist = log.Contains("Set new TxFFE"))) //等待執行檔回傳"Set new TxFFE"後才可以繼續往下做
                {
                    break;
                }
                Thread.Sleep(100);
            }

                 
            
            for(int input_number = 0; input_number < 4; input_number++)
            {
                p.StandardInput.WriteLine(input_number.ToString());
                Thread.Sleep(1000);
                if (input_number == 0)
                {
                    Check_DSAZ334A_state(visa_name.Text, ":PDER?");
                    DSAZ334A(visa_name.Text, ":SINGle");
                    Check_DSAZ334A_state(visa_name.Text, ":PDER?");
                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"" + freq + port + LAN + "_" + input_number.ToString() + "\",PNG,SCR,ON,NORMal");//存檔檔名不能使用小數點
                    //Thread.Sleep(30000);
                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"" + freq + port + LAN + "_" + input_number.ToString() + "\",BIN,ON");
                    Thread.Sleep(30000);
                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"" + freq + port + LAN + "_" + input_number.ToString() + "\",BIN,ON");
                    Thread.Sleep(30000);
                    
                }
                else
                {
                    Check_DSAZ334A_state(visa_name.Text, ":PDER?");
                    DSAZ334A(visa_name.Text, ":SINGle");
                    Check_DSAZ334A_state(visa_name.Text, ":PDER?");
                    //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"" + freq + port + LAN + "_" + input_number.ToString() + "\",PNG,SCR,ON,NORMal");//存檔檔名不能使用小數點
                    //Thread.Sleep(1000);
                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel1,\"" + freq + port + LAN + "_" + input_number.ToString() + "\",BIN,ON");
                    Thread.Sleep(30000);
                    DSAZ334A(visa_name.Text, ":DISK:SAVE:WAVeform CHANnel2,\"" + freq + port + LAN + "_" + input_number.ToString() + "\",BIN,ON");
                    Thread.Sleep(30000);
                }
                //Check_DSAZ334A_state(visa_name.Text, ":PDER?");
                //DSAZ334A(visa_name.Text, ":SINGle");
                //Check_DSAZ334A_state(visa_name.Text, ":PDER?");
                //Check_DSAZ334A_state(visa_name.Text, ":PDER?");
                //DSAZ334A(visa_name.Text, ":DISK:SAVE:IMAGe \"" + freq + port + LAN + "_" + input_number.ToString() + "\",PNG,SCR,ON,NORMal");//存檔檔名不能使用小數點


            }

            p.StandardInput.WriteLine("q");
            p.StandardInput.WriteLine("exit");
            //log = p.StandardOutput.ReadToEnd();
            p.WaitForExit();
            p.Close();
        }

        [DllImport("User32.dll")]
        static extern int SetForegroundWindow(IntPtr point);
        void excute_cmd_tx_CLK(string parameter)
        {
            Process p = new Process();
            String log = null;
            bool string_exist;
            
            p.StartInfo.FileName = "USB4ElectricalTestToolCLI.exe";
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardInput = true;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.RedirectStandardError = true;
            p.StartInfo.CreateNoWindow = true; //不跳出cmd視窗
            p.StartInfo.Arguments = parameter;
            if (tgl_mode.Checked == true)
            {
                p.StartInfo.Arguments = parameter + " -tgl";
            }
            else
            {
                p.StartInfo.Arguments = parameter;
            }
            p.Start();
            var pointer = p.Handle;
            //if (tgl_mode.Checked == true)
            //{
            //    p.StandardInput.WriteLine(cmd_command + " -tgl");
            //}
            //else
            //{
            //    p.StandardInput.WriteLine(cmd_command);
            //}
            while (true)
            {
                log = p.StandardOutput.ReadLine();
                if(log != null)
                {
                    this.Invoke((MethodInvoker)delegate ()
                    {
                        Log.Items.Add(log);
                        Log.TopIndex = Log.Items.Count - 1;
                    });
                    if ((string_exist = log.Contains("Push <Enter>")))
                    {
                        Thread.Sleep(1000);
                        
                        break;
                    }
                    Thread.Sleep(100);
                }
            }
            //Thread.Sleep(10000);
            //log = p.StandardOutput.ReadToEnd();
            
            DSAZ334A(visa_name.Text, ":SINGle");
            Thread.Sleep(2000);
            SetForegroundWindow(pointer);
            Thread.Sleep(100);


            
            
            SendKeys.SendWait("{ENTER}");
            //SendKeys.SendWait("~");
            
            p.StandardInput.WriteLine("");
           

            p.StandardInput.WriteLine("exit");
            p.WaitForExit();
            p.Close();
            Check_DSAZ334A_state(visa_name.Text, ":PDER?");
        }

        void ctle_excel_write(int row,int col, double value, string lan)
        {
            string FileStr ;

            if (lan == "lan0")
                FileStr = file_path + "CTLE\\CTLE" + now_time + "_L0" + ".xlsx";
            else
                FileStr = file_path + "CTLE\\CTLE" + now_time + "_L1" + ".xlsx";

            Excel.Application Excel_app = new Excel.Application();
            Excel_app.DisplayAlerts = false;
            Excel.Workbook Excel_WB1 = Excel_app.Workbooks.Open(FileStr);

            Excel_app.Cells[row, col] = value;
            Excel_WB1.Save();
            Excel_WB1.Close();
            Excel_WB1 = null;
            Excel_app.Quit();
            Excel_app = null;
        }
        void creat_new_ctle_excel(string lan)
        {
            string FileStr = file_path + "CTLE.xlsx";
            //if (lan1.Checked == true && lan0.Checked == true)
            //{
            //    Excel.Application Excel_app = new Excel.Application();
            //    Excel_app.DisplayAlerts = false;
            //    Excel.Workbook Excel_WB1 = Excel_app.Workbooks.Open(FileStr);
            //    Excel_app.Cells[1, 2] = "";
            //    Excel_WB1.SaveAs(file_path + "CTLE\\CTLE" + now_time + "_L0" + ".xlsx");
            //    Excel_WB1.Close();
            //    Excel_WB1 = null;
            //    Excel_app.Quit();
            //    Excel_app = null;

            //    Excel.Application Excel_app2 = new Excel.Application();
            //    Excel_app2.DisplayAlerts = false;
            //    Excel.Workbook Excel_WB2 = Excel_app2.Workbooks.Open(FileStr);
            //    Excel_app2.Cells[1, 2] = "";
            //    Excel_WB2.SaveAs(file_path + "CTLE\\CTLE" + now_time + "_L1" + ".xlsx");
            //    Excel_WB2.Close();
            //    Excel_WB2 = null;
            //    Excel_app2.Quit();
            //    Excel_app2 = null;
            //}
            if(lan == "lan1" )
            {
                Excel.Application Excel_app = new Excel.Application();
                Excel_app.DisplayAlerts = false;
                Excel.Workbook Excel_WB1 = Excel_app.Workbooks.Open(FileStr);
                Excel_app.Cells[1, 2] = "";
                Excel_WB1.SaveAs(file_path + "CTLE\\CTLE" + now_time + "_L1" + ".xlsx");
                Excel_WB1.Close();
                Excel_WB1 = null;
                Excel_app.Quit();
                Excel_app = null;
            }
            else
            {
                Excel.Application Excel_app = new Excel.Application();
                Excel_app.DisplayAlerts = false;
                Excel.Workbook Excel_WB1 = Excel_app.Workbooks.Open(FileStr);
                Excel_app.Cells[1, 2] = "";
                Excel_WB1.SaveAs(file_path + "CTLE\\CTLE" + now_time + "_L0" + ".xlsx");
                Excel_WB1.Close();
                Excel_WB1 = null;
                Excel_app.Quit();
                Excel_app = null;
            }
            
        }
        void DSAZ3341_excute_end()
        {
            Check_DSAZ334A_state(visa_name.Text, ":PDER?");
            Thread.Sleep(500);
            while (true) //檢查run是否完成
            {
                Check_DSAZ334A_state(visa_name.Text, ":PDER?");
                Thread.Sleep(100);

                if (PDER_value == "1")
                {
                    break;
                }

            }
        }
        private void Log_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void port_a_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void port_b_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void port_d_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void lan1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void lan2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void test11_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Log.Items.Clear();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var rmSession = new ResourceManager();
            try
            {
                mbSession_DSAZ334A = (MessageBasedSession)rmSession.Open(visa_name.Text);
                mbSession_DSAZ334A.RawIO.Write("*RST");
                mbSession_DSAZ334A.Dispose();
                MessageBox.Show("SUCCESS : Connect to Instrument Pass");
                //r6522_read_value = InsertCommonEscapeSequences(mbSession_DSAZ334A.RawIO.ReadString()).Substring(4, 6);
            }
            catch
            {
                MessageBox.Show("WARNING : Connect to Instrument Fail");
                MessageBox.Show(visa_name.Text);
            }


        }

        private void TEST_FLOW_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (button3.Text == "Matlab analysis")
            {
                BackColor = Color.FromArgb(255, 128, 128);
                analysising = new Thread(analysis);
                analysising.Start();
                button3.Text = "analysising";
            }
            else
            {
                BackColor = Color.FromArgb(255, 228, 163);
                button3.Text = "Matlab analysis";
                analysising.Abort();
            }
        }

        private void analysis()
        {
            Process p1 = new Process();
            p1.StartInfo.FileName = "CIO_TX_SSC.exe";
            p1.StartInfo.UseShellExecute = false;
            p1.StartInfo.RedirectStandardInput = true;
            p1.StartInfo.RedirectStandardOutput = true;
            p1.StartInfo.RedirectStandardError = true;
            p1.StartInfo.CreateNoWindow = true; //跳出cmd視窗



            Process p2 = new Process();
            p2.StartInfo.FileName = "CLK_SW_v1p1.exe";
            //p2.StartInfo.FileName = "cmd.exe";
            p2.StartInfo.UseShellExecute = false;
            p2.StartInfo.RedirectStandardInput = true;
            p2.StartInfo.RedirectStandardOutput = true;
            p2.StartInfo.RedirectStandardError = true;
            p2.StartInfo.CreateNoWindow = true; //跳出cmd視窗

            string d;
            string f;
            string n;
            string log = null;
            string[] empty = new string[1];
            //string[] lines = new string[8];
            int lines_i;
            empty[0] = " ";
            DirectoryInfo di = new DirectoryInfo(file_path + "\\Matlab_analysis");
            foreach (var doc in di.GetFiles("*.bin"))
            {
                string[] lines = new string[50];
                bool start_log = false;
                d = doc.ToString();
                n = d.Remove(d.Length - 6, 6);
                d = "Matlab_analysis\\" + d;
                f = d.Replace(".bin", "");
                lines_i = 0;
                if (d.Contains("SSC"))
                {
                    switch (Convert.ToDouble(Regex.Replace(n, "[^0-9]", "")))
                    {
                        case 10:
                            p1.StartInfo.Arguments = "\"" + d + "\"" + " " + "10000000000";
                            p1.Start();
                            while (true)
                            {
                                try
                                {
                                    log = p1.StandardOutput.ReadLine();
                                }
                                catch
                                {
                                    log = null;
                                }
                                if (log != null)
                                {
                                    if (log.Contains("SSC_DOWN_SPREAD_RATE"))
                                    {
                                        start_log = true;
                                    }
                                    if (start_log)
                                    {
                                        lines[lines_i] = log;
                                        lines_i++;
                                    }
                                }
                                else
                                {
                                    start_log = false;
                                    break;
                                }
                            }
                            p1.WaitForExit();
                            break;
                        case 103:
                            p1.StartInfo.Arguments = "\"" + d + "\"" + " " + "10312500000";
                            p1.Start();
                            while (true)
                            {
                                try
                                {
                                    log = p1.StandardOutput.ReadLine();
                                }
                                catch
                                {
                                    log = null;
                                }
                                if (log != null)
                                {
                                    if (log.Contains("SSC_DOWN_SPREAD_RATE"))
                                    {
                                        start_log = true;
                                    }
                                    if (start_log)
                                    {
                                        lines[lines_i] = log;
                                        lines_i++;
                                    }
                                }
                                else
                                {
                                    start_log = false;
                                    break;
                                }
                            }
                            p1.WaitForExit();
                            break;
                        case 20:
                            p1.StartInfo.Arguments = "\"" + d + "\"" + " " + "20000000000";
                            p1.Start();
                            while (true)
                            {
                                try
                                {
                                    log = p1.StandardOutput.ReadLine();
                                }
                                catch
                                {
                                    log = null;
                                }
                                if (log != null)
                                {
                                    if (log.Contains("SSC_DOWN_SPREAD_RATE"))
                                    {
                                        start_log = true;
                                    }
                                    if (start_log)
                                    {
                                        lines[lines_i] = log;
                                        lines_i++;
                                    }
                                }
                                else
                                {
                                    start_log = false;
                                    break;
                                }
                            }
                            p1.WaitForExit();
                            break;
                        case 206:
                            p1.StartInfo.Arguments = "\"" + d + "\"" + " " + "20625000000";
                            p1.Start();
                            while (true)
                            {
                                try
                                {
                                    log = p1.StandardOutput.ReadLine();
                                }
                                catch
                                {
                                    log = null;
                                }
                                if (log != null)
                                {
                                    if (log.Contains("SSC_DOWN_SPREAD_RATE"))
                                    {
                                        start_log = true;
                                    }
                                    if (start_log)
                                    {
                                        lines[lines_i] = log;
                                        lines_i++;
                                    }
                                }
                                else
                                {
                                    start_log = false;
                                    break;
                                }
                            }
                            p1.WaitForExit();
                            break;
                    }
                    System.IO.File.WriteAllLines(f + ".txt", empty);
                    StreamWriter sw = new StreamWriter(f + ".txt", true);
                    for (int i = 0; i < 48; i++)
                    {
                        sw.WriteLine("");
                    }
                    for (int i = 0; i < lines_i; i++)
                    {
                        sw.WriteLine(lines[i]);
                    }
                    sw.Close();
                }
                else
                {//CLK_SW
                    switch (Convert.ToDouble(Regex.Replace(n, "[^0-9]", "")))
                    {
                        case 10:
                            p2.StartInfo.Arguments = "\"" + d + "\"" + " " + "10000000000";
                            p2.Start();
                            while (true)
                            {
                                try
                                {
                                    log = p2.StandardOutput.ReadLine();
                                }
                                catch
                                {
                                    log = null;
                                }
                                if (log != null)
                                {
                                    lines[lines_i] = log;
                                    lines_i++;
                                }
                                else
                                {
                                    break;
                                }
                            }
                            p2.WaitForExit();
                            break;
                        case 103:
                            p2.StartInfo.Arguments = "\"" + d + "\"" + " " + "10312500000";
                            p2.Start();
                            while (true)
                            {
                                try
                                {
                                    log = p2.StandardOutput.ReadLine();
                                }
                                catch
                                {
                                    log = null;
                                }
                                if (log != null)
                                {
                                    lines[lines_i] = log;
                                    lines_i++;
                                }
                                else
                                {
                                    break;
                                }
                            }
                            p2.WaitForExit();
                            break;
                        case 20:
                            p2.StartInfo.Arguments = "\"" + d + "\"" + " " + "20000000000";
                            p2.Start();
                            while (true)
                            {
                                try
                                {
                                    log = p2.StandardOutput.ReadLine();
                                }
                                catch
                                {
                                    log = null;
                                }
                                if (log != null)
                                {
                                    lines[lines_i] = log;
                                    lines_i++;
                                }
                                else
                                {
                                    break;
                                }
                            }
                            p2.WaitForExit();
                            break;
                        case 206:
                            p2.StartInfo.Arguments = "\"" + d + "\"" + " " + "20625000000";
                            p2.Start();
                            while (true)
                            {
                                try
                                {
                                    log = p2.StandardOutput.ReadLine();
                                }
                                catch
                                {
                                    log = null;
                                }
                                if (log != null)
                                {
                                    lines[lines_i] = log;
                                    lines_i++;
                                }
                                else
                                {
                                    break;
                                }
                            }
                            p2.WaitForExit();
                            break;
                    }
                    System.IO.File.WriteAllLines(f + ".txt", lines);
                }

            }
            p1.Close();
            p2.Close();
            this.Invoke((MethodInvoker)delegate ()
            {
                button3.Text = "Matlab analysis";
                BackColor = Color.FromArgb(255, 228, 163);
            });
        }

        private void Freq20G_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void test1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void test10_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void visa_name_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void tgl_mode_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void test2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void test8_CheckedChanged(object sender, EventArgs e)
        {

        }
        private void test7_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void test7_CheckedChanged_1(object sender, EventArgs e)
        {

        }
    }
}
